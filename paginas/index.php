
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?>    

    

    <!--    ==============================================================  -->
    <!--    ROYAL SLIDER    -->
    <!--    ==============================================================  -->
    <!-- slider JS files -->
    <script  src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery-1.8.0.min.js"></script>
    <link href="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/royalslider.css" rel="stylesheet">
    <script src="<?php echo Util::caminho_projeto() ?>/jquery/cc-royalslider-9.2.0/royalslider/jquery.royalslider.min.js"></script>
    <script>
        jQuery(document).ready(function($) {
                // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
                // it's recommended to disable them when using autoHeight module
                $('#content-slider-1').royalSlider({
                    autoHeight: true,
                    arrowsNav: true,
                    arrowsNavAutoHide: false,
                    fadeinLoadedSlide: false,
                    controlNavigationSpacing: 0,
                    controlNavigation: 'bullets',
                    imageScaleMode: 'none',
                    imageAlignCenter: false,
                    loop: true,
                    loopRewind: true,
                    numImagesToPreload: 6,
                    keyboardNavEnabled: true,
                    usePreloader: false,
                    autoPlay: {
                        // autoplay options go gere
                        enabled: true,
                        pauseOnHover: true
                    }

                });
            });
</script>     



<script>
    $(window).load(function() {
        $('.flexslider').flexslider({
          animation: "slide",
          animationLoop: false,
          itemWidth: 190,
          prevText: "",
          nextText: ""

        });
    });
  </script>





</head>
<body>



  



  
    <!-- ==============================================================  -->
    <!--  BANNERS   -->
    <div class="container-fluid">
        <div class="row slide-personalizado">
            <div id="container_banner">
               <div id="content_slider">
                  <div id="content-slider-1" class="contentSlider rsDefault">

                    <!-- ITEM --> 
                    <div>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-home.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/banner-2.jpg" alt="">
                    </div>
                    <div>
                        <img src="<?php echo Util::caminho_projeto() ?>/imgs/banner-3.jpg" alt="">
                    </div>
                    <!-- FIM DO ITEM -->

                </div>
            </div>
        </div>
    </div>
</div>    
<!--  BANNERS   -->
<!--  ==============================================================  -->





<!--  ==============================================================  -->
<!-- conteudo -->
<div class="pagina">



    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->


    <div class="container top395">
        <div class="row">
            <div class="col-xs-8">
                <div class="categorias-home ">
                    <h1>O QUE VOCÊ PRECISA:</h1>
                      

                      <!-- Place somewhere in the <body> of your page -->
                      <div class="flexslider">
                        <ul class="lista-categorias slides top35">
                            
                            <?php
                            $result = $obj_site->select("tb_categorias_produtos");
                            if(mysql_num_rows($result) > 0){
                              while($row = mysql_fetch_array($result)){
                              ?>
                                <li>
                                   <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>">
                                    <?php Util::imprime($row[titulo]) ?>
                                  </a>
                                </li>
                              <?php 
                              }
                            } 
                            ?>
                        </ul>
                      </div>
                      <!-- items mirrored twice, total of 12 -->


                      <?php /* ?>
                      <div id="slider" class="flexslider text-center">
                        <ul class="lista-categorias slides top35">
                      
                          
                            <?php
                            $result = $obj_site->select("tb_categorias_produtos");
                            if(mysql_num_rows($result) > 0){
                              while($row = mysql_fetch_array($result)){
                              ?>
                                <li>
                                   <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>"> <br>
                                    <?php Util::imprime($row[titulo]) ?>
                                  </a>
                                </li>
                              <?php 
                              }
                            } 
                            ?>
                        </ul>
                      </div>
                      <?php */ ?>
   
                 </div>
            </div>



        <!-- pesquisa produtos -->
        <div class="col-xs-4 pt10 text-center pesquisa-home">
            <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-pesquisa-produtos-home.jpg" alt="">
                <select class="form-control top20 input-lg" name="busca_categoria">
                  <option value="">CATEGORIA</option>
                  <?php
          $result = $obj_site->select("tb_categorias_produtos");
          if(mysql_num_rows($result) > 0){
            while($row = mysql_fetch_array($result)){
            ?>
              <option value="<?php Util::imprime($row[0]) ?>"><?php Util::imprime($row[titulo]) ?></option>
            <?php
              }
            }
            ?>
              </select>
              <input class="form-control input-lg top10" type="text" name="busca_descricao" placeholder="BUSCAR POR NOME">

              <!-- botao de pesquisa -->
              <div class="text-right top20 bottom50">
                <button type="submit" class="btn-pesquisa">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-buscar-produtos.jpg" alt="">
                </button>
                
            </div>
            <!-- botao de pesquisa -->

          </form>

    </div>
    <!-- pesquisa produtos -->
  </div>
</div>


<!-- lista de produtos home -->
<div class="container">
    <div class="row">

          <?php
          $result = $obj_site->select("tb_produtos", "order by rand() limit 6 ");
          if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
                ?>


                <!-- produto 01 -->
                <div class="col-xs-4 listagem-produtos">
                    <div class="thumbnail effect6 pt15">
                          <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Saiba mais">
                              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>" class="input100">
                          </a>
                          <div class="caption">
                            <h1 class="top30"><?php Util::imprime($row[titulo]) ?></h1>
                            <h2 class="top10"><?php Util::imprime($row[descricao], 200) ?></h2>
                            

                            <!-- botao saiba mais -->
                            <div class="posicao-imagem text-center">
                                <a class="btn btn-roxo top15" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                    ADICIONAR AO ORÇAMENTO 
                                    <i class="fa fa-cart-plus"></i>
                                </a>
                            </div>
                            <!-- botao saiba mais -->
                            </div>
                    </div> 
                </div>
                <!-- produto 01 -->
                <?php 
                }
              }
              ?>


        <!-- botao mais produtos -->
        <div class="col-xs-12 text-right bottom100">
            <a href="<?php echo Util::caminho_projeto() ?>/produtos">
                 <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-maisProduto-home.jpg" alt="">
            </a>
        </div>
        <!-- botao mais produtos -->



    </div>
</div>
<!-- lista de produtos home -->


<!-- conheca mais a imperio -->
<div class="container-fluid fundo-branco ">
    <div class="row bg-saiba-mais">
        <div class="container">
            <div class="row">
                <div class="col-xs-6  top40 ">
                    <div class="conhecaImperio">
                        <h3>CONHEÇA MAIS A IMPÉRIO</h3>
                        <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
                        <a href="<?php echo Util::caminho_projeto() ?>/empresa">
                          <p class="top15"><?php Util::imprime($dados[descricao], 500) ?></p>
                        </a>

                        <h3 class="top30">CONFIRA OS SERVIÇOS</h3>
                            
                            <?php
                            $result = $obj_site->select("tb_servicos", "order by rand() limit 3 ");
                            if(mysql_num_rows($result) > 0){
                              while($row = mysql_fetch_array($result)){
                                ?>
                                  <a href="<?php echo Util::caminho_projeto() ?>/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                                    <h2 class="top15 bottom10">
                                      <?php Util::imprime($row[titulo]) ?>
                                    </h2>
                                  </a>
                                <?php 
                                }
                              }
                              ?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- conheca mais a imperio -->

<!-- nossas dicas home -->
<div class="container top90">
    <div class="row">
          
          <?php
          $result = $obj_site->select("tb_dicas", "order by rand() limit 3");
          if(mysql_num_rows($result) > 0){
            $i = 0;
            while($row = mysql_fetch_array($result)){
              
              if ($i == 0) {
              ?>
                <!-- dicas01 -->
                <div class="col-xs-4 listagem-dicas">
                    <div class="thumbnail">
                          <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 373, 370); ?>
                            <div class="caption">
                              <h2 class="top10"><?php Util::imprime($row[titulo]) ?></h2>
                           </div>
                         </a>
                    </div> 
                </div>
              <?php
              }else{
              ?>
                <!-- dicas02 -->
                <div class="col-xs-4 listagem-dicas <?php if($i > 1){ echo 'top111'; } ?>">
                    
                    <?php if($i == 1): ?>
                      <h1>CONFIRA TODAS AS</h1>
                      <h3>NOSSAS DICAS</h3>
                    <?php endif; ?>
                    
                    <div class="thumbnail">
                          <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]) ?>">
                              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 372, 253); ?>
                            <div class="caption">
                              <h2 class="top10"><?php Util::imprime($row[titulo]) ?></h2>
                           </div>
                         </a>
                    </div> 
                </div>
              <?php
              }

              $i++;

            ?>

            <?php 
            }
          }
          ?>
          

         

    </div>
</div>
<!-- nossas dicas home -->

<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</div>


<!-- conteudo -->
<!--  ==============================================================  -->






</body>
</html>


