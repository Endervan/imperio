
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?> 



</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->



  <div class="container top40">
    <div class="row titulo-pagina">
            <!-- titulo da pagina -->
            <div class="col-xs-5 text-right">
                <h1>CONHEÇA NOSSOS</h1>
                <h2>PRODUTOS</h2>
            </div>
            <!-- titulo da pagina -->

            <!-- pesquisa produtos -->
            <?php require_once("./includes/busca.php"); ?> 
            <!-- pesquisa produtos -->

        </div>
        <!-- pesquisa produtos -->
    </div>
  </div>







  <!-- menu produtos geral -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">

        <!-- categorias da home -->
        <div class="categorias-home ">
            <ul class="lista-categorias">
              
              <li>
                 <a href="<?php echo Util::caminho_projeto() ?>/produtos/" title="Todas as categorias">
                  <br><img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-cinza.png"  alt="Todas as categorias"> <br><br>
                  TODAS AS CATEGORIAS
                </a>
              </li>

                          
              <?php
              $url1 = Url::getURL(1);

              $result = $obj_site->select("tb_categorias_produtos");
              if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                ?>
                  <li class="<?php if($url1 == $row[url_amigavel]){ echo 'active'; } ?>">
                     <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>"> <br>
                      <?php Util::imprime($row[titulo]) ?>
                    </a>
                  </li>
                <?php 
                }
              } 
              ?>
          </ul>
        </div>
        <!-- categorias da home -->

    </div>

  </div>
</div>
<!-- menu produtos geral -->



  <!-- produto geral -->
  <div class="container top20 bottom40">
    <div class="row">
           
            
           <?php
           $url1 = Url::getURL(1);

          //  FILTRA AS CATEGORIAS
          if (isset( $url1 )) {
              $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url1);
              $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
          }

          //  FILTRA PELO TITULO
          if(isset($_POST[busca_descricao])  and !empty($_POST[busca_descricao]) ):
            $complemento = "AND titulo LIKE '%$_POST[busca_descricao]%'";
          endif;

          //  FILTRA PELa categoria VIA POST
          if(isset($_POST[busca_categoria]) and !empty($_POST[busca_categoria]) ):
            $complemento .= "AND id_categoriaproduto = '$_POST[busca_categoria]' ";
          endif;



          $result = $obj_site->select("tb_produtos", $complemento);
          if(mysql_num_rows($result) == 0)
          {
            echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
          }else{
              while($row = mysql_fetch_array($result))
              {
              ?>


            <!-- produto 01 -->
            <div class="col-xs-4 listagem-produtos">
                <div class="thumbnail effect6 pt15">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Saiba mais">
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>" class="input100">
                      </a>
                      <div class="caption">
                        <h1 class="top30"><?php Util::imprime($row[titulo]) ?></h1>
                        <h2 class="top10"><?php Util::imprime($row[descricao], 200) ?></h2>
                        

                        <!-- botao saiba mais -->
                        <div class="posicao-imagem text-center">
                            <a class="btn btn-roxo top15" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                ADICIONAR AO ORÇAMENTO 
                                <i class="fa fa-cart-plus"></i>
                            </a>
                        </div>
                        <!-- botao saiba mais -->
                        </div>
                </div> 
            </div>
            <!-- produto 01 -->
            <?php 
            }
          }
          ?>




    </div>
  </div>
  <!-- produto geral -->




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>

