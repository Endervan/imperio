<?php  


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
   $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>



<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?> 
  <!-- FlexSlider -->
  <script defer src="http://masmidia.com.br/clientes/san-remo/jquery/woothemes-FlexSlider-83b3cae/jquery.flexslider.js"></script>
  <!-- Syntax Highlighter -->
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shCore.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushXml.js"></script>
  <script type="text/javascript" src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/shBrushJScript.js"></script>

  <!-- Optional FlexSlider Additions -->
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.easing.js"></script>
  <script src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/jquery.mousewheel.js"></script>
  <script defer src="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/demo/js/demo.js"></script>


  <!-- Syntax Highlighter -->
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shCore.css" rel="stylesheet" type="text/css" />
  <link href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/shThemeDefault.css" rel="stylesheet" type="text/css" />


  <!-- Demo CSS -->
  <link rel="stylesheet" href="<?php echo Util::caminho_projeto() ?>/jquery/woothemes-FlexSlider-83b3cae/css/flexslider.css" type="text/css" media="screen" />


  <script>
    // Can also be used with $(document).ready()
    $(window).load(function() {
      $('.flexslider').flexslider({
        animation: "slide"
      });
    });
  </script>


<!-- Rating
http://plugins.krajee.com/star-rating
================================================== -->
<link href="<?php echo Util::caminho_projeto() ?>/dist/css/star-rating.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php echo Util::caminho_projeto() ?>/dist/js/star-rating.min.js" type="text/javascript"></script>

<script>
jQuery(document).ready(function () {

    $('.avaliacao').rating({
          min: 0,
          max: 5,
          step: 1,
          size: 'xs',
          showClear: false,
          disabled: true,
          clearCaption: 'Seja o primeiro a avaliar.',
          starCaptions: {
                            0.5: 'Half Star',
                            1: 'Ruim',
                            1.5: 'One & Half Star',
                            2: 'Regular',
                            2.5: 'Two & Half Stars',
                            3: 'Bom',
                            3.5: 'Three & Half Stars',
                            4: 'Ótimo',
                            4.5: 'Four & Half Stars',
                            5: 'Excelente'
                        }
       });


});
</script>


</head>
<body class="bg-produtos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->



  <div class="container top40">
        <div class="row titulo-pagina">
                <!-- titulo da pagina -->
                <div class="col-xs-5 text-right">
                    <h1>CONHEÇA NOSSOS</h1>
                    <h2>PRODUTOS</h2>
                </div>
                <!-- titulo da pagina -->

                <!-- pesquisa produtos -->
                <?php require_once("./includes/busca.php"); ?> 
                <!-- pesquisa produtos -->

            </div>
            <!-- pesquisa produtos -->
        </div>
        
      </div>

  <!-- menu produtos geral -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        
        <!-- categorias da home -->
        <div class="categorias-home ">
            <ul class="lista-categorias ">
              
              <li>
                 <a href="<?php echo Util::caminho_projeto() ?>/produtos/" title="Todas as categorias">
                  <br><img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-cinza.png"  alt="Todas as categorias"> <br><br>
                  TODAS AS CATEGORIAS
                </a>
              </li>

                          
              <?php
              $url1 = Url::getURL(1);

              $result = $obj_site->select("tb_categorias_produtos");
              if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){
                ?>
                  <li class="<?php if($url1 == $row[url_amigavel]){ echo 'active'; } ?>">
                     <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                      <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>"> <br>
                      <?php Util::imprime($row[titulo]) ?>
                    </a>
                  </li>
                <?php 
                }
              } 
              ?>
          </ul>
        </div>
        <!-- categorias da home -->

</div>

</div>
</div>
<!-- menu produtos geral -->



<!-- produto geral -->
<div class="container top20 bottom40">
  <div class="row">
   <!-- descricao do produto -->
   <div class="col-xs-5 slider-produtos">

        <!-- Place somewhere in the <body> of your page -->
        <div class="flexslider">
          <ul class="slides">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0){
              while($row = mysql_fetch_array($result)){
              ?>
              <li>
                  <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 450, 450, array("class"=>"", "alt"=>"")) ?>
              </li>
            <?php 
              }
            }
            ?>  

          </ul>
        </div>


 





</div>

<!-- descricao produtos dentro -->
<div class="col-xs-7">
  <div class="descricao-dentro">
    <h1><?php Util::imprime($dados_dentro[titulo]) ?></h1>

    <p class="top20"><?php Util::imprime($dados_dentro[descricao]) ?></p>

    <div class="text-right top45">
      <a class="top20" href="javascript:void(0);" title="Solicite um orçamento" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/botao-adicionaOrcamento.png" />
      </a>
    </div>
  </div>

</div>
<!-- descricao produtos dentro -->



</div>
</div>
<!-- produto geral -->


<div class="clearfix"></div>

<!-- avaliacao de produto -->
<div class="container top50">
  <div class="row">
    <div class="col-xs-5">
      <div class="avaliacoes">
        <h2 class="left270">CONFIRA AS</h2>
        <h4>AVALIAÇÕES DO PRODUTO</h4>
      </div> 
    </div>


    <?php
    $result = $obj_site->select("tb_avaliacoes_produtos", "AND id_produto = '$dados_dentro[0]' ORDER BY data DESC");

    if(mysql_num_rows($result) > 0)
    {
        while($row = mysql_fetch_array($result))
        {
        ?>
        <div class="col-xs-12 top20">
          <div class="col-xs-3 top10">
            <h1><?php Util::imprime($row[nome]) ?></h1>  
            <!-- avaliacao -->
            <div class="descricao-produtos text-left">
              <input id="avaliaca-1" class="avaliacao" type="number" value="<?php Util::imprime($row[nota]) ?>" />
            </div>
            <!-- avaliacao -->
          </div>
           <div class="col-xs-9">
            <p><?php Util::imprime($row[comentario]) ?></p>
           </div>
        </div>
        <?php
        }
    }else{
      echo "<div class=\"clearfix\"></div><h1 class='top50'>Nenhum comentário. Seja o primeiro a comentar.</h1>";
    }
    ?>
 



    

    <!-- form comentario -->
            <div class="clearfix"></div>
            <div class="top50">
                <?php
                if (isset($_POST[comentario])) {
                  $obj_site->insert("tb_avaliacoes_produtos");
                  Util::alert_bootstrap("Muito obrigado pelo seu comentário.");
                }
                ?>
                <form class="form-inline FormContato" role="form" method="post">

                      <div class="row">
                          <div class="col-xs-4 form-group">
                              <label class="glyphicon glyphicon-user"> Nome</label>
                              <input type="text" name="nome" class="form-control input100" placeholder="">
                          </div>
                          <div class="col-xs-4 form-group">
                              <label class="glyphicon glyphicon-envelope"> E-mail</label>
                              <input type="text" name="email" class="form-control input100" placeholder="">
                          </div>
                          <div class="col-xs-4 form-group">
                              <label class="fa fa-star"> Nota</label>
                              <select name="nota" id="nota" class="form-control input100">
                                  <option value="">Selecione</option>
                                  <option value="5">Excelente</option>
                                  <option value="4">Ótimo</option>
                                  <option value="3">Bom</option>
                                  <option value="2">Regular</option>
                                  <option value="1">Ruim</option>
                              </select>
                          </div>

                          <div class="col-xs-12 top20 form-group">
                              <label class="glyphicon glyphicon-pencil"> Comentário</label>
                              <textarea name="comentario" id="" cols="30" rows="10" class="form-control input100"></textarea>
                          </div>

                          <div class="clearfix"></div>

                          <input type="hidden" name="id_produto" value="<?php echo $dados_dentro[0] ?>">
                          <input type="hidden" name="ativo" value="NAO">
                          <input type="hidden" name="data" value="<?php echo date("d/m/Y") ?>">

                          <div class="text-right top30">
                              <button type="submit" class="btn btn-default">
                                  ENVIAR
                              </button>
                          </div>
                        </div>

                    </form>
            </div>
              <!-- form comentario -->


  </div>
</div>
<!-- avaliacao de produto -->



















<!-- veja tambem -->
<div class="container top60 bottom200">
  <div class="row">
    <div class="col-xs-12">
      <div class="veja-tambem">
        <h2 class="left80">VEJA </h2>
        <h4>TAMBÉM</h4>
      </div>
    </div>

      
  
      <?php
      $result = $obj_site->select("tb_produtos", "AND idproduto <> $dados_dentro[0] order by rand() limit 3 ");
      if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            ?>


            <!-- produto 01 -->
            <div class="col-xs-4 listagem-produtos">
                <div class="thumbnail effect6 pt15">
                      <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]) ?>" data-toggle="tooltip" data-placement="top" title="Saiba mais">
                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="<?php Util::imprime($row[titulo]) ?>" class="input100">
                      </a>
                      <div class="caption">
                        <h1 class="top30"><?php Util::imprime($row[titulo]) ?></h1>
                        <h2 class="top10"><?php Util::imprime($row[descricao], 200) ?></h2>
                        

                        <!-- botao saiba mais -->
                        <div class="posicao-imagem text-center">
                            <a class="btn btn-roxo top15" href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Adicionar ao orçamento" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                                ADICIONAR AO ORÇAMENTO 
                                <i class="fa fa-cart-plus"></i>
                            </a>
                        </div>
                        <!-- botao saiba mais -->
                        </div>
                </div> 
            </div>
            <!-- produto 01 -->
            <?php 
            }
          }
          ?>

  </div>
</div>
<!-- veja tambem -->




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>





<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {

                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                nota: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                comentario: {
                    validators: {
                        notEmpty: {

                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {

                        }
                    }
                }
            }
        });
    });
</script>


