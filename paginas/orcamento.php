<?php
//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
    }

}
?>


<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?> 



</head>
<body class="bg-orcamentos">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->



  <div class="container top40">
    <div class="row titulo-pagina">
            <!-- titulo da pagina -->
            <div class="col-xs-5 text-right">
                <h1>SOLICITE UM</h1>
                <h2>ORÇAMENTO</h2>
            </div>
            <!-- titulo da pagina -->

            <!-- pesquisa produtos -->
            <?php require_once("./includes/busca.php"); ?> 
            <!-- pesquisa produtos -->

        </div>
        <!-- pesquisa produtos -->
    </div>
  </div>







  <form class="form-inline FormContato" role="form" method="post">

  <!-- itens do carrinho -->
  <div class="container">
    <div class="row">


      <?php
      //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
      if(isset($_POST[nome]))
      {

          //  CADASTRO OS PRODUTOS SOLICITADOS
          for($i=0; $i < count($_POST[qtd]); $i++)
          {
              $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

              $mensagem .= "
                          <tr>
                              <td><p>". $_POST[qtd][$i] ."</p></td>
                              <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                           </tr>
                          ";
          }




          //  ENVIANDO A MENSAGEM PARA O CLIENTE
          $texto_mensagem = "
          O seguinte cliente fez uma solicitação pelo site. <br />


          Nome: $_POST[nome] <br />
          Email: $_POST[email] <br />
          Telefone: $_POST[telefone] <br />
          Cidade: $_POST[cidade] <br />
          Estado: $_POST[estado] <br />
          Mensagem: <br />
          ".nl2br($_POST[mensagem])." <br />


          <br />
          <h2> Produtos selecionados:</h2> <br />

          <table width='100%' border='0' cellpadding='5' cellspacing='5'>

              <tr>
                    <td><h4>QTD</h4></td>
                    <td><h4>PRODUTO</h4></td>
              </tr>

              $mensagem

          </table>


          </table>
          ";

          //echo $texto_mensagem; exit;

          Util::envia_email($config[email], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
          Util::envia_email($config[email_copia], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
          unset($_SESSION[solicitacoes_produtos]);
          unset($_SESSION[solicitacoes_servicos]);
          Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");

      }
      ?>

      


      <?php
      if(count($_SESSION[solicitacoes_produtos]) > 0){
        for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
          $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
          ?>
          <!-- itens orcamentos 01 -->
          <div class="col-xs-12 bottom20">
            <div class="fundo-cinza1">
              <div class="col-xs-2">
                <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100" >
              </div>
              <div class="col-xs-5 top40">
                <p><?php Util::imprime($row[titulo]) ?></p>
              </div>
              <div class="col-xs-2 top35">
                <p>QDT.<input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                  <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"/>
                </p>
              </div>
              <div class="col-xs-3 top25">
                <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="">
                  <img src="<?php echo Util::caminho_projeto() ?>/imgs/retira-orcamento.png" alt="">
                </a>
              </div>
            </div>
          </div>
          <!-- itens orcamentos 01-->
        <?php 
        }
      }
      ?>




      <!-- continue orcando -->
      <div class="col-xs-12">
        <a href="<?php echo Util::caminho_projeto() ?>/produtos" title="Continuar orçando" class="btn btn-primary">
          Continuar orçando
        </a>
      </div>
      <!-- continue orcando -->
      
    </div>
  </div>
  <!-- itens do carrinho -->

  <!-- servicos desejavel -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12 top30 bottom10">
        <div class="descricao-orcamento">
          <h3>CONFIRME SEUS DADOS</h3>
        </div>
      </div>

      <!-- formulario orcamento -->
      <div class="FormContato1 col-xs-12 bottom100">
        <div class="row">
          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
            <input type="text" name="nome" class="form-control fundo-form1 input100" placeholder="">
          </div>

          <div class="col-xs-6 form-group ">
            <label class="glyphicon glyphicon-user"> <span>E-mail</span></label>
            <input type="text" name="email" class="form-control fundo-form1 input100" placeholder="">
          </div>
        </div>

        <div class="row">
          <div class="col-xs-6 form-group">
            <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
            <input type="text" name="telefone" class="form-control fundo-form1 input100" placeholder="">
          </div>

          <div class="col-xs-6 form-group">
           <label class="glyphicon glyphicon-star"> <span>Tipo de pessoa</span></label>
           <input type="text" name="tipo_pessoa" class="form-control fundo-form1 input100" placeholder="">
         </div>
       </div>

       <div class="row">
        <div class="col-xs-6 form-group">
          <label <i class="fa fa-globe"></i> <span>Cidade</span></label>
          <input type="text" name="cidade" class="form-control fundo-form1 input100" placeholder="">
        </div>


        <div class="col-xs-6 form-group">
         <label <i class="fa fa-globe"></i><span>Estado</span></label>
         <input type="text" name="estado" class="form-control fundo-form1 input100" placeholder="">
       </div>
     </div>

     <div class="row">
      <div class="col-xs-12 form-group">
        <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
        <textarea name="mensagem" id="" cols="30" rows="8" class="form-control  fundo-form1 input100" placeholder=""></textarea>
      </div>
    </div>

    <div class="clearfix"></div>

    <div class="text-right top30 bottom40">
      <button type="submit" class="btn btn-azul" name="btn_trabalhe_conosco">
        ENVIAR
      </button>
    </div>
  </div>
  <!-- formulario orcamento -->

 


</div>
</div>


</form>




<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>



<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      tipo_pessoa: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>