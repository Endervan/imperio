
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <?php require_once('./includes/head.php'); ?> 

    
<!-- intervalo carroucel    -->
<script>
    $(document).ready( function() {
    $('#carousel-example-generic').carousel({
        interval:5000
    });
});

</script>
<!-- intervalo carroucel    -->


    
</head>
<body class="bg-empresa">

    <!-- topo -->
    <?php require_once('./includes/topo.php') ?>
    <!-- topo -->



     <div class="container top60">
        <div class="row titulo-pagina">
                <!-- titulo da pagina -->
                <div class="col-xs-5 text-right">
                    <h1>CONHEÇA NOSSA</h1>
                    <h2>EMPRESA</h2>
                </div>
                <!-- titulo da pagina -->

                <!-- pesquisa produtos -->
                <?php require_once("./includes/busca.php"); ?> 
                <!-- pesquisa produtos -->

            </div>
            <!-- pesquisa produtos -->
        </div>
        
      </div>





<!-- descricao empresa geral -->
<div class="container">
    <div class="row ">
        <div class="col-xs-7 descricao-empresa">
            <?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
            <p><?php Util::imprime($dados[descricao]) ?></p>

        </div>

        <div class="col-xs-5">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-descricao-empresa.png" alt="">
        </div>
    </div>
</div>
<!-- descricao empresa geral -->








<!-- rodape -->
<?php require_once('./includes/rodape.php') ?>
<!-- rodape -->

</body>
</html>
