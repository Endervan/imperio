
<!DOCTYPE html>
<html lang="pt-br">
<head>
  <?php require_once('./includes/head.php'); ?> 


  <!-- intervalo carroucel    -->
  <script>
    $(document).ready( function() {
      $('#carousel-example-generic').carousel({
        interval:5000
      });
    });

  </script>
  <!-- intervalo carroucel    -->



</head>
<body class="bg-contato">

  <!-- topo -->
  <?php require_once('./includes/topo.php') ?>
  <!-- topo -->



  <div class="container top60">
    <div class="row titulo-pagina">
      <!-- titulo da pagina -->
      <div class="col-xs-7 text-right">
        <h1>ENTRE EM CONTATO</h1>
        <h2>FALE CONOSCO</h2>
      </div>
      <!-- titulo da pagina -->

      <!-- pesquisa produtos -->
      <div class="col-xs-4 col-xs-offset-1 pt10 text-center pesquisa-home">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-pesquisa-produtos-home.jpg" alt="">
        <select class="form-control top20 input-lg">
          <option>CATEGORIA</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </select>
        <input class="form-control input-lg top10" type="text" placeholder="BUSCAR POR NOME">

        <!-- botao de pesquisa -->
        <div class="text-right top20 bottom50">
          <a href="">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/bg-buscar-produtos.jpg" alt="">
          </a>
        </div>
        <!-- botao de pesquisa -->

      </div>
      <!-- pesquisa produtos -->
    </div>
  </div>





  <!-- contatos geral -->
      <div class="container  menu-empresa ">
        <div class="row">
          <div class="col-xs-8 col-xs-offset-4  ">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs text-center menu-contatos left81 " role="tablist">
              <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">FALE CONOSCO</a></li>
              <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TRABALHE CONOSCO</a></li>
            </ul>
          </div>

          <div class="col-xs-7 fundo-branco col-xs-offset-5 ">
            <!-- Tab panes -->
            <div class="tab-content fundo-branco">

              <!-- fale conosco -->
              <div role="tabpanel" class="tab-pane fade in active" id="home">
                <?php  
                            //  VERIFICO SE E PARA ENVIAR O EMAIL
                if(isset($_POST[btn_contato]))
                {
                  $nome_remetente = ($_POST[nome]);
                  $email = ($_POST[email]);
                  $assunto = ($_POST[assunto]);
                  $telefone = ($_POST[telefone]);
                  $mensagem = (nl2br($_POST[mensagem]));
                  $texto_mensagem = "
                  Nome: $nome_remetente <br />
                        Assunto: $assunto <br />
                        Telefone: $telefone <br />
                        Email: $email <br />
                        Mensagem: <br />
                        $mensagem
                  ";
                  Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                  Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                  Util::alert_bootstrap("Obrigado por entrar em contato.");
                  unset($_POST);
                }
                ?>

                <form class=" col-xs-12 form-inline FormContato top60" role="form" method="post">

                  <div class="row">
                    <div class="col-xs-6 form-group ">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-6 form-group">
                      <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                      <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                      <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                      <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-xs-12 top20 form-group">
                      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                      <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                    </div>

                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right top30 bottom40">
                    <button type="submit" class="btn btn-azul" name="btn_contato">
                      ENVIAR
                    </button>
                  </div>


                </form>

              </div>

              <!-- fale conosco -->



              <!-- trabalhe conosco -->
              <div role="tabpanel" class="tab-pane fade" id="profile">
                <?php  
                              //  VERIFICO SE E PARA ENVIAR O EMAIL
                if(isset($_POST[btn_trabalhe_conosco]))
                {
                  $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
                  $assunto = Util::trata_dados_formulario($_POST[assunto]);
                  $email = Util::trata_dados_formulario($_POST[email]);
                  $telefone = Util::trata_dados_formulario($_POST[telefone]);
                  $escolaridade = Util::trata_dados_formulario($_POST[escolaridade]);
                  $cargo = Util::trata_dados_formulario($_POST[cargo]);
                  $area = Util::trata_dados_formulario($_POST[area]);
                  $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));

                  if(!empty($_FILES[curriculo][name])):
                      $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
                      $texto = "Anexo: ";
                      $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
                      $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
                  endif;

                  $texto_mensagem = "
                                    Nome: $nome_remetente <br />
                                    Assunto: $assunto <br />
                                    Telefone: $telefone <br />
                                    Email: $email <br />
                                    Escolaridade: $escolaridade <br />
                                    Cargo: $cargo <br />
                                    Área: $area <br />
                                    Mensagem: <br />
                                    $texto    <br><br>
                                    $mensagem
                                    ";


                  Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
                  Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
                  Util::alert_bootstrap("Obrigado por entrar em contato.");
                  unset($_POST);
                }
                ?>

                <form class=" col-xs-12 form-inline FormCurriculo top60" role="form" method="post" enctype="multipart/form-data">

                  <div class="row">
                    <div class="col-xs-6 form-group ">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-6 form-group">
                      <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                      <input type="text" name="email" class="form-control fundo-form input100" placeholder="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                      <input type="text" name="telefone" class="form-control fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-star"> <span>Assunto</span></label>
                      <input type="text" name="assunto" class="form-control fundo-form input100" placeholder="">
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-file"> <span>Currículo</span></label>
                      <input type="file" name="curriculo" class="form-control fundo-form input100" placeholder="">
                    </div>
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-book"> <span>Escolaridade</span></label>
                      <input type="text" name="escolaridade" class="form-control fundo-form input100" placeholder="">
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-lock"> <span>Cargo</span></label>
                      <input type="text" name="cargo" class="form-control fundo-form input100" placeholder="">
                    </div>

                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-briefcase"> <span>Area</span></label>
                      <input type="text" name="area" class="form-control fundo-form input100" placeholder="">
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-globe"> <span>Cidade</span></label>
                      <input type="text" name="cidade" class="form-control fundo-form input100" placeholder="">
                    </div>


                    <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-globe"> <span>Estado</span></label>
                      <input type="text" name="estado" class="form-control fundo-form input100" placeholder="">
                    </div>
                  </div>
                  


                  <div class="row">
                    <div class="col-xs-12 top20 form-group">
                      <label class="glyphicon glyphicon-pencil"> <span>Sua Mensagem</span></label>
                      <textarea name="mensagem" id="" cols="30" rows="10" class="form-control  fundo-form input100" placeholder=""></textarea>
                    </div>

                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right top30 bottom40">
                    <button type="submit" class="btn btn-azul" name="btn_azul">
                      ENVIAR
                    </button>
                  </div>

                </form> 
              </div>
              <!-- trabalhe conosco -->


            </div>
            <!-- Tab panes -->
          </div>

        </div>
      </div> 
  <!-- contatos geral -->

  <!-- saiba como chegar -->
  <div class="container-fluid posicao">
    <div class="row">
      <div class="container bottom200">
        <div class="row">
          <div class="col-xs-12 top40 text-right">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/como-chegar.png" alt="">
            <iframe src="<?php Util::imprime($config[src_place]) ?>" width="1014" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- saiba como chegar -->


  <!-- rodape -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- rodape -->

</body>
</html>




<script>
  $(document).ready(function() {
    $('.FormContato').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
  });
</script>



<script>
  $(document).ready(function() {
    $('.FormCurriculo').bootstrapValidator({
      message: 'This value is not valid',
      feedbackIcons: {
        valid: 'glyphicon glyphicon-ok',
        invalid: 'glyphicon glyphicon-remove',
        validating: 'glyphicon glyphicon-refresh'
      },
      fields: {
       nome: {
        validators: {
          notEmpty: {

          }
        }
      },
      email: {
        validators: {
          notEmpty: {

          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {

          }
        }
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      },
      escolaridade: {
        validators: {
          notEmpty: {

          }
        }
      },
      cidade: {
        validators: {
          notEmpty: {

          }
        }
      },
      estado: {
        validators: {
          notEmpty: {

          }
        }
      },
      cargo: {
        validators: {
          notEmpty: {

          }
        }
      },
      area: {
        validators: {
          notEmpty: {

          }
        }
      },
      curriculo: {
        validators: {
          notEmpty: {
            message: 'Por favor insira seu currículo'
          },
          file: {
            extension: 'doc,docx,pdf,rtf',
            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                          }
                        }
                      },
                      mensagem: {
                        validators: {
                          notEmpty: {

                          }
                        }
                      }
                    }
                  });
});
</script>
