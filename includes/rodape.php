<div class="clearfix"></div>
<div class="container-fluid rodape-preto">
	<div class="row">

		<!-- menu -->
		<div class="container top20">
			<div class="row">
				<div class="col-xs-8">
					<ul class="menu-rodape">
						<li><a href="<?php echo Util::caminho_projeto() ?>/">HOME</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/empresa">EMPRESA</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/servicos">SERVICO</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS</a></li>
						<li><a href="<?php echo Util::caminho_projeto() ?>/contatos">CONTATOS</a></li>

					</ul>
				</div>
			</div>
		</div>
		<!-- menu -->

		<!-- logo, endereco, telefone -->
		<div class="container top20">
			<div class="row">
				
				<div class="col-xs-10 contatos-rodape">
					<p><i class="glyphicon glyphicon-home"></i><?php Util::imprime($config[endereco]) ?></p>
					<p class="top10"><i class="glyphicon glyphicon-phone"></i>
						<?php 

						if (!empty($config[telefone1])){
			            	Util::imprime($config[telefone1]);
			          	} 

			          	if (!empty($config[telefone2])) {
			            	Util::imprime(" / ".$config[telefone2]) ;
			          	} 

			          	if (!empty($config[telefone3])) {
			            	Util::imprime(" / ".$config[telefone3]) ;
			          	} 

			          	 ?>

					</p>
				</div>

				<div class="col-xs-2 text-right pb15">

					<?php if ($config[google_plus] != "") { ?>
						<a class="pull-left top15 left50"  href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<p style="color: #fff"><i class="fa fa-google-plus"></i></p>
						</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo-masmidia.png" alt="">
					</a>
				</div>

			</div>
		</div>
		<!-- logo, endereco, telefone -->

	</div>
</div>

<div class="container-fluid">
	<div class="row">
		<div class="rodape-roxo">
			<h5 class="text-center">TODOS OS DIREITOS RESERVADOS </h5>
		</div>
	</div>
</div>

