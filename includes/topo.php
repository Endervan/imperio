

<div class="container-fluid topo">
  <div class="row">
    <div class="container">
      <div class="row topo-telefone">

        <div class="col-xs-3">
          <img src="<?php echo Util::caminho_projeto() ?>/imgs/central-topo.png" alt="">
        </div>
        <!-- contatos topo -->
        <div class="col-xs-6 padding0">

          <?php if (!empty($config[telefone1])) { ?>
            <div class="col-xs-4">
              <h2><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone-topo.png" alt=""><?php Util::imprime($config[telefone1]) ?></h2>
            </div>
          <?php } ?>


          <?php if (!empty($config[telefone2])) { ?>
            <div class="col-xs-4">
              <h2><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone-topo.png" alt=""><?php Util::imprime($config[telefone2]) ?></h2>
            </div>
          <?php } ?>

          <?php if (!empty($config[telefone3])) { ?>
            <div class="col-xs-4">
              <h2><img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-telefone-topo.png" alt=""><?php Util::imprime($config[telefone3]) ?></h2>
            </div>
          <?php } ?>


        </div>
        <!-- contatos topo -->

        <!-- menu topo -->
        <div class="col-xs-3">
          <div class="dropdown">
            <a id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-meu-orcamento">
            <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn-orcamento-topo.png" alt="">
            </a>
            <div class="dropdown-menu topo-meu-orcamento pull-right" aria-labelledby="dLabel">

              <h6 class="bottom20">MEU ORÇAMENTO(<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h6>

              <?php
              if(count($_SESSION[solicitacoes_produtos]) > 0)
              {
                  for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                  {
                      $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                      ?>
                      <div class="lista-itens-carrinho">
                          <div class="col-xs-2">
                              <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="46" width="29" alt="">
                          </div>
                          <div class="col-xs-8">
                              <h1><?php Util::imprime($row[titulo]) ?></h1>
                          </div>
                          <div class="col-xs-1">
                              <a href="<?php echo Util::caminho_projeto() ?>/orcamento/?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> <i class="glyphicon glyphicon-remove"></i> </a>
                          </div>
                      </div>
                      <?php
                  }
              }
              ?>


              <div class="text-right bottom20">
                <a href="<?php echo Util::caminho_projeto() ?>/orcamento" title="Finalizar" class="btn btn-azul" >
                  FINALIZAR
                </a>
              </div>


            </div>

          </div>
        </div>
        <!-- menu topo -->
      </div>
    </div>
  </div>
</div>


<div class="container top60">
  <div class="row">
  <!-- logo -->
    <div class="col-xs-3">
      <a href="<?php echo Util::caminho_projeto() ?>">
        <img src="<?php echo Util::caminho_projeto() ?>/imgs/logo.jpg" alt="">
      </a>
    </div>
    <!-- logo -->


    <!-- menu topo -->
    <div class="col-xs-9 navs-personalizados">
      <!-- menu -->
      <nav class="navbar navbar-default navbar-right" role="navigation">
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
         <ul class="nav navbar-nav">
          <li class="active">
           <a href="<?php echo Util::caminho_projeto() ?>">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-home-topo.png" alt=""> <br>
              HOME
          </a>
          </li>

          <li>
            <a href="<?php echo Util::caminho_projeto() ?>/empresa">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-empresa.png" alt=""><br>
              A EMPRESA
          </a>
          </li>

          <li>
             <a href="<?php echo Util::caminho_projeto() ?>/servicos">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-servico.png" alt=""><br>
                SERVIÇOS
              </a>
          </li>

          <li>
             <a href="<?php echo Util::caminho_projeto() ?>/produtos">
                <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-produtos.png" alt=""><br>
              PRODUTOS
            </a>
          </li>

          <li>
           <a href="<?php echo Util::caminho_projeto() ?>/dicas">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-dicas.png" alt=""><br>
              DICAS
          </a>
          </li>

            <li>
            <a href="<?php echo Util::caminho_projeto() ?>/contatos">
              <img src="<?php echo Util::caminho_projeto() ?>/imgs/icon-contatos.png" alt=""><br>
              CONTATOS
            </a>
          </li>

        </ul>
        </div>
        <!-- /.navbar-collapse -->
        </nav>
      </div>
      <!-- menu topo-->
 </div>
</div>
