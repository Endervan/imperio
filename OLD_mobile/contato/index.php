<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
<?php require_once('../includes/head.php'); ?>


</head>


</head>

<body class="bg-contato">
    

    <?php require_once('../includes/topo.php'); ?>


	  <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->
    <div class="container pesquisa-prod-internas">
      <div class="row">
        <div class="col-xs-6 bg-cinza bottom15">
          <img class="top10" src="../imgs/titulo-pesquisar-produtos.jpg" alt="">   
        </div>
        
        <div class="clearfix"></div>

        <?php require_once("../includes/form_busca.php") ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->

    


    <!-- lista produtos -->
    <div class="container bg-form-contato">
        <div class="row top35">
          
          <div class="col-xs-8 col-xs-offset-4 tabs-cont">
            <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">FALE CONOSCO</a></li>
                  <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">TRABALHE CONOSCO</a></li>
                </ul>

          </div>

          <div class="col-xs-8 col-xs-offset-4 tabs-contatos bg-branco">
              
              
              <div>

                
              

                <!-- Tab panes -->
                <div class="tab-content top30 ">
                  
                  <!-- fale conosco -->
                  <div role="tabpanel" class="tab-pane fade in active" id="home">


                      <?php  
                      //  VERIFICO SE E PARA ENVIAR O EMAIL
                      if(isset($_POST[btn_contato]))
                      {
                          $nome_remetente = ($_POST[nome]);
                          $email = ($_POST[email]);
                          $assunto = ($_POST[assunto]);
                          $telefone = ($_POST[telefone]);
                          $mensagem = (nl2br($_POST[mensagem]));
                          $texto_mensagem = "
                                                            Nome: $nome_remetente <br />
                                                            Assunto: $assunto <br />
                                                            Telefone: $telefone <br />
                                                            Email: $email <br />
                                                            Mensagem: <br />
                                                            $mensagem
                                                            ";
                          Util::envia_email($config[email], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                          Util::envia_email($config[email_copia], utf8_decode($assunto), $texto_mensagem, utf8_decode($nome_remetente), $email);
                          Util::alert_bootstrap("Obrigado por entrar em contato.");
                          unset($_POST);
                      }
                      ?>

                      <form class="form-inline FormContato" role="form" method="post">

                        <div class="row">
                            <div class="col-xs-12 form-group">
                                <input type="text" name="nome" class="form-control input100" placeholder="NOME">
                            </div>
                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="email" class="form-control input100" placeholder="EMAIL">
                            </div>

                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="telefone" class="form-control input100" placeholder="TELEFONE">
                            </div>
                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="assunto" class="form-control input100" placeholder="ASSUNTO">
                            </div>

                            <div class="col-xs-12 top10 form-group">
                                <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100" placeholder="MENSAGEM" ></textarea>
                            </div>

                            <div class="clearfix"></div>

                            <div class="text-right top30 bottom20 right20">
                                <button type="submit" class="btn btn-vermelho" name="btn_contato">
                                    ENVIAR
                                </button>
                            </div>
                          </div>

                      </form>
                  </div>
                  <!-- fale consco -->

                  

                  <!-- trabalhe conosco -->
                  <div role="tabpanel" class="tab-pane fade" id="profile">
                      <?php  
                      //  VERIFICO SE E PARA ENVIAR O EMAIL
                      if(isset($_POST[btn_trabalhe_conosco]))
                      {
                        $nome_remetente = Util::trata_dados_formulario($_POST[nome]);
                        $assunto = Util::trata_dados_formulario($_POST[assunto]);
                        $email = Util::trata_dados_formulario($_POST[email]);
                        $telefone = Util::trata_dados_formulario($_POST[telefone]);
                        $mensagem = Util::trata_dados_formulario(nl2br($_POST[mensagem]));
                        
                        if(!empty($_FILES[curriculo][name])):
                          $nome_arquivo = Util::upload_arquivo("./uploads", $_FILES[curriculo]);
                          $texto = "Anexo: ";
                          $texto .= "Clique ou copie e cole o link abaixo no seu navegador de internet para visualizar o arquivo.<br>";
                          $texto .= "<a href='".Util::caminho_projeto()."/uploads/$nome_arquivo' target='_blank'>".Util::caminho_projeto()."/uploads/$nome_arquivo</a>";
                        endif;
              
                              $texto_mensagem = "
                                                Nome: $nome_remetente <br />
                                                Assunto: $assunto <br />
                                                Telefone: $telefone <br />
                                                Email: $email <br />
                                                Mensagem: <br />
                                                $texto    <br><br>
                                                $mensagem
                                                ";
                                   
                              
                              Util::envia_email($config[email], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
                              Util::envia_email($config[email_copia], "CURRÍCULO PELO SITE ".$_SERVER[SERVER_NAME], $texto_mensagem, $nome_remetente, $email);
                              Util::alert_bootstrap("Obrigado por entrar em contato.");
                              unset($_POST);
                      }
                      ?>

                      <form class="form-inline FormCurriculo" role="form" method="post" enctype="multipart/form-data">
                          <div class="row">
                            <div class="col-xs-12 form-group">
                                <input type="text" name="nome" class="form-control input100" placeholder="NOME">
                            </div>
                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="email" class="form-control input100" placeholder="EMAIL">
                            </div>

                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="telefone" class="form-control input100" placeholder="TELEFONE">
                            </div>
                            <div class="col-xs-12 top10 form-group">
                                <input type="text" name="assunto" class="form-control input100" placeholder="ASSUNTO">
                            </div>

                            <div class="col-xs-12 top10 form-group">
                                <input type="file" name="curriculo" class="form-control input100" placeholder="">
                            </div>

                            <div class="col-xs-12 top10 form-group">
                                <textarea name="mensagem" id="" cols="30" rows="10" class="form-control input100" placeholder="MENSAGEM"></textarea>
                            </div>

                            <div class="clearfix"></div>

                            <div class="text-right top30 bottom20 right20">
                                <button type="submit" class="btn btn-vermelho" name="btn_trabalhe_conosco">
                                  ENVIAR
                              </button>
                            </div>
                          </div>
                        </form> 
                  </div>
                  <!-- trabalhe conosco -->
                  
                  

                </div>

              </div>



          </div>


          


        </div>
    </div>



  <div class="container">
    <div class="row">
       <div class="col-xs-12 top15">
            <iframe src="<?php Util::imprime($config[src_place]) ?>" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>    
          </div> 
    </div>
  </div>

    




    















    <?php require_once('../includes/rodape.php'); ?>

</body>
</html>







<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                tipo_pessoa: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                cidade: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                estado: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>










<script>
    $(document).ready(function() {
        $('.FormCurriculo').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                curriculo: {
                    validators: {
                        notEmpty: {
                            message: 'Por favor insira seu currículo'
                        },
                        file: {
                            extension: 'doc,docx,pdf,rtf',
                            type: 'application/pdf,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/rtf',
                            maxSize: 5*1024*1024,   // 5 MB
                            message: 'O arquivo selecionado não é valido, ele deve ser (doc,docx,pdf,rtf) e 5 MB no máximo.'
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>



