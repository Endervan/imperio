<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
<?php require_once('../includes/head.php'); ?>


</head>

<body class="bg-produtos">
    

    <?php require_once('../includes/topo.php'); ?>


    <!-- texto chamada -->
    <div class="container">
        <div class="row text-center titulo-internas">
            <h6>NOSSO PORTFÓLIO</h6>
        </div>        
    </div>
    <!-- texto chamada -->  


  



    <!-- lista produtos -->
    <div class="container bg-branco">
        <div class="row top35">
            <?php 
            $result = $obj_site->select("tb_portifolios");
            if(mysql_num_rows($result) == 0)
            {
              echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
            }else{
                while($row = mysql_fetch_array($result))
                {
                ?>
                    <div class="col-xs-12 lista-port">
                        <a href="<?php Util::imprime($row[url_amigavel]) ?>">
                            <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 435, 230, array('class'=>'input100')); ?>
                        </a>
                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                        <p><?php Util::imprime($row[descricao], 300) ?></p>
                        <a href="<?php Util::imprime($row[url_amigavel]) ?>" class="btn btn-default">SAIBA MAIS</a>
                    </div>
                <?php 
                }
            }
            ?>
        </div>
    </div>
    <!-- lista produtos -->



    <?php require_once('../includes/rodape.php'); ?>

</body>
</html>







<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>

