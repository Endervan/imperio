<form class="pesquisa-prod" action="<?php echo Util::caminho_projeto() ?>/mobile/produtos/" method="post">
  <div class="col-xs-4">
    <?php Util::cria_select_bd("tb_categorias_produtos", "idcategoriaproduto", "titulo", "busca_categoria", "", "input100", 'CATEGORIA') ?>
  </div>
  <div class="col-xs-5">
    <input type="text" name="busca_produtos" class="input100" placeholder="BUSCAR POR NOME">
  </div>
  <div class="col-xs-3">
    <button class="border-none">
      <img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-bucar.jpg" alt="">
    </button>
  </div>
</form>