<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
<?php require_once('../includes/head.php'); ?>

<script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 20,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 130,
        itemMargin: 27,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>




  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 20,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 130,
        itemMargin: 27,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>


</head>

<body class="bg-produtos">
    

    <?php require_once('../includes/topo.php'); ?>


    
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->
    <div class="container pesquisa-prod-internas">
      <div class="row">
        <div class="col-xs-6 bg-cinza bottom15">
          <img class="top10" src="../imgs/titulo-pesquisar-produtos.jpg" alt="">   
        </div>
        
        <div class="clearfix"></div>

        <?php require_once("../includes/form_busca.php") ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!-- categorias -->
    <!-- ======================================================================= -->
    <div class="container top25">
        <div class="row">
            <div class="col-xs-12">
                <div id="slider" class="flexslider text-center">
                    <ul class="lista-categorias slides">
                        <?php
                          $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 4");
                          if(mysql_num_rows($result) > 0){
                            while($row = mysql_fetch_array($result)){
                            ?>
                              <li>
                                 <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[0]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
                                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>">
                                  <?php Util::imprime($row[titulo]) ?>
                                </a>
                              </li>
                            <?php 
                            }
                          } 
                          ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- ======================================================================= -->
    <!-- categorias -->
    <!-- ======================================================================= -->




    <!-- ======================================================================= -->
    <!-- listagem dos produtos  -->
    <!-- ======================================================================= -->
    <div class="container top50">
        <div class="row">
            <?php
            //  titulo
            if (isset($_POST[busca_produtos])) {
                $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
            }

            //  categoria
            if (isset($_GET[cat])) {
                $complemento .= "AND id_categoriaproduto = '$_GET[cat]'";
            }


            $result = $obj_site->select("tb_produtos", $complemento);
            if(mysql_num_rows($result) == 0)
            {
              echo "<h2 class='bg-info' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
            }else{
                while($row = mysql_fetch_array($result)){
                ?>
                <div class="col-xs-6 lista-produto">
                    <div class="thumbnail">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
                        <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
                      </a>
                      <div class="caption">
                        <h1><?php Util::imprime($row[titulo]) ?></h1>
                        <p><?php Util::imprime($row[descricao], 200) ?></p>
                        
                        <a href="javascript:void(0);" class="btn btn-azul-orc" role="button" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                            ADICIONAR AO ORÇAMENTO
                        </a> 

                      </div>
                    </div>
                </div>
                <?php 
                }
            }
            ?>


        </div>
    </div>
    <!-- ======================================================================= -->
    <!-- listagem dos produtos  -->
    <!-- ======================================================================= -->




    <?php require_once('../includes/rodape.php'); ?>

</body>
</html>







    
