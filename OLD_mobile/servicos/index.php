<?php
require_once("../../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
<?php require_once('../includes/head.php'); ?>

<script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 20,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 130,
        itemMargin: 27,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>




  <script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 20,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 130,
        itemMargin: 27,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>


</head>

<body class="bg-servicos">
    

    <?php require_once('../includes/topo.php'); ?>


    
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->
    <div class="container pesquisa-prod-internas">
      <div class="row">
        <div class="col-xs-6 bg-cinza bottom15">
          <img class="top10" src="../imgs/titulo-pesquisar-produtos.jpg" alt="">   
        </div>
        
        <div class="clearfix"></div>

        <?php require_once("../includes/form_busca.php") ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->






    <!-- ======================================================================= -->
    <!-- listagem dos produtos  -->
    <!-- ======================================================================= -->
    <div class="container top50">
        <div class="row">
            
            <?php
              $result = $obj_site->select("tb_servicos");
              if(mysql_num_rows($result) > 0){
                while($row = mysql_fetch_array($result)){

                  if (++$i == 1) {
                  ?>
                    <div class="col-xs-12 bottom40 lista-servico">
                        <div class="media"> 
                          <div class="media-left">
                            <?php if(!empty($row[imagem])){ ?>
                              <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200, 200); ?>
                              </a>
                            <?php } ?>  
                          </div>
                        
                          <div class="media-body">
                             <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                               <h4 class="media-heading"><?php Util::imprime($row[titulo]) ?></h4>
                               <p><?php Util::imprime($row[descricao], 450) ?></p>
                             </a>
                          </div>
                        </div>
                    </div>
                  <?php
                    $i = 1;
                  }else{
                  ?>
                    <div class="col-xs-12 bottom40 lista-servico">
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                        <div class="media">
                          <div class="media-body">
                            <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                              <h4 class="media-heading"><?php Util::imprime($row[titulo]) ?></h4>
                              <p><?php Util::imprime($row[descricao], 450) ?></p>
                            </a>
                          </div>
                          <div class="media-right">
                            <?php if(!empty($row[imagem])){ ?>
                              <a href="<?php echo Util::caminho_projeto() ?>/mobile/servicos/<?php Util::imprime($row[url_amigavel]) ?>">
                                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 200, 200); ?>
                              </a>
                            <?php } ?>  
                          </div>
                        </div>
                      </a>
                    </div>
                  <?php
                    $i = 0;
                  }

                ?>
              
                <?php 
                }
              }
              ?>


        </div>
    </div>
    <!-- ======================================================================= -->
    <!-- listagem dos produtos  -->
    <!-- ======================================================================= -->




    <?php require_once('../includes/rodape.php'); ?>

</body>
</html>







    
