<?php
require_once("../class/Include.class.php"); 
$obj_site = new Site();
?>
<!doctype html>
<html>
<head>
<?php require_once('./includes/head.php'); ?>


<script>
    $(window).load(function() {
      // The slider being synced must be initialized first
      $('#carousel').flexslider({
        animation: "slide",
        controlNav: false,
        animationLoop: false,
        slideshow: false,
        itemWidth: 130,
        itemMargin: 20,
        asNavFor: '#slider'
      });

      $('#slider').flexslider({
        animation: "slide",
        customDirectionNav: $(".custom-navigation a"),
        controlNav: true,
        itemWidth: 130,
        itemMargin: 27,
        animationLoop: false,
        slideshow: false,
        sync: "#carousel"
      });
    });
  </script>

</head>


</head>

<body>
    

    <?php require_once('./includes/topo.php'); ?>

	


	<!-- ======================================================================= -->
	<!-- slider -->
	<!-- ======================================================================= -->
	<div class="container margin-slider">
		<div class="row">
			
			
			
			<div id="carousel-example-generic" class="carousel slide slider-index" data-ride="carousel">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
			    
	                    
	                    <li data-target="#carousel-example-generic" data-slide-to="<?php echo $i; ?>" class="<?php echo $active ?>"></li>
	                    
	                   
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">

			   
	                    <div class="item <?php echo $active ?>">
	                    	
									<a href="<?php Util::imprime($imagem[url]) ?>">
										<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
									</a>
	                    		
									<img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($imagem[imagem]) ?>" alt="">
	                    		
	                    </div>
	                    

			  </div>
					

				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>



			</div>


		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- slider -->
	<!-- ======================================================================= -->





	<!-- ======================================================================= -->
	<!-- pesquisa produto	-->
	<!-- ======================================================================= -->
	<div class="container pesquisa-prod-index">
		<div class="row">
			<div class="col-xs-6 bg-cinza bottom15">
				<img class="top10" src="./imgs/titulo-pesquisar-produtos.jpg" alt="">		
			</div>
			
			<div class="clearfix"></div>

			<?php require_once("./includes/form_busca.php") ?>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- pesquisa produto	-->
	<!-- ======================================================================= -->



	
	<!-- ======================================================================= -->
	<!-- categorias	-->
	<!-- ======================================================================= -->
	<div class="container top25">
		<div class="row">
			<div class="col-xs-12">
				<div id="slider" class="flexslider text-center">
					<ul class="lista-categorias slides">
						<?php
		                  $result = $obj_site->select("tb_categorias_produtos", "ORDER BY rand() LIMIT 4");
		                  if(mysql_num_rows($result) > 0){
		                    while($row = mysql_fetch_array($result)){
		                    ?>
		                      <li>
		                         <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/?cat=<?php Util::imprime($row[0]) ?>" title="<?php Util::imprime($row[titulo]) ?>">
		                          <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]) ?>"  alt="<?php Util::imprime($row[titulo]) ?>">
		                          <?php Util::imprime($row[titulo]) ?>
		                        </a>
		                      </li>
		                    <?php 
		                    }
		                  } 
		                  ?>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- categorias	-->
	<!-- ======================================================================= -->
	



	<!-- ======================================================================= -->
	<!-- listagem dos produtos	-->
	<!-- ======================================================================= -->
	<div class="container top50">
		<div class="row">
			  <?php
	          $result = $obj_site->select("tb_produtos", "ORDER BY rand() LIMIT 4");
	          if(mysql_num_rows($result) > 0){
	            while($row = mysql_fetch_array($result)){
	            ?>
				<div class="col-xs-6 lista-produto">
					<div class="thumbnail">
				      <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos/<?php Util::imprime($row[url_amigavel]) ?>">
				      	<img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
				      </a>
				      <div class="caption">
				        <h1><?php Util::imprime($row[titulo]) ?></h1>
						<p><?php Util::imprime($row[descricao], 200) ?></p>
				        
				        <a href="javascript:void(0);" class="btn btn-azul-orc" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
							ADICIONAR AO ORÇAMENTO
				        </a> 

				      </div>
				    </div>
				</div>
				<?php 
				}
			}
			?>


			<div class="col-xs-12 top10 text-center">
				<a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos">
					<img src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn-mais-produtos.jpg" alt="">
				</a>
			</div>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- listagem dos produtos	-->
	<!-- ======================================================================= -->


	<!-- ======================================================================= -->
	<!-- conheca a imperio	-->
	<!-- ======================================================================= -->
	<div class="container bg-conheca top20">
		<div class="row">
			<div class="col-xs-8 top25">
				<h1>CONHEÇA MAIS A IMPÉRIO</h1>
				<?php $dados= $obj_site->select_unico("tb_empresa", "idempresa", 1) ?>
	            <a href="<?php echo Util::caminho_projeto() ?>/mobile/empresa">
	              <p class="top15 conheca-p"><?php Util::imprime($dados[descricao], 500) ?></p>
	            </a>

	            <h1 class="top15">CONFIRA OS SERVIÇOS</h1>
	            <?php
                $result = $obj_site->select("tb_servicos", "order by rand() limit 3 ");
                if(mysql_num_rows($result) > 0){
                  while($row = mysql_fetch_array($result)){
                    ?>
                      <a href="<?php echo Util::caminho_projeto() ?>/mobile/servico/<?php Util::imprime($row[url_amigavel]) ?>">
                        <p class="top10 bottom10">
                          <?php Util::imprime($row[titulo]) ?>
                        </p>
                      </a>
                    <?php 
                    }
                  }
                  ?>

			</div>
		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- conheca a imperio	-->
	<!-- ======================================================================= -->

	



	<!-- ======================================================================= -->
	<!-- dicas	-->
	<!-- ======================================================================= -->
	<div class="container top40 dicas">
		<div class="row">
			<div class="col-xs-6 text-right">
				<h1>CONFIRA TODAS AS</h1>
				<h1><span>NOSSAS DICAS</span></h1>
			</div>

			<div class="clearfix bottom20"></div>
			
			<?php
	          $result = $obj_site->select("tb_dicas", "ORDER BY rand() LIMIT 2");
	          if(mysql_num_rows($result) > 0){
	            while($row = mysql_fetch_array($result)){
	            ?>
				<div class="col-xs-6 lista-produto">
					<div class="thumbnail">
					  <a href="<?php echo Util::caminho_projeto() ?>/mobile/dicas/<?php Util::imprime($row[url_amigavel]) ?>">
					      <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]) ?>" alt="" class="input100">
					      <div class="caption">
					        <h1><?php Util::imprime($row[titulo]) ?></h1>
					      </div>
				      </a>
				    </div>
				</div>
				<?php 
				}
			}
			?>

		</div>
	</div>
	<!-- ======================================================================= -->
	<!-- dicas	-->
	<!-- ======================================================================= -->

		






    <?php require_once('./includes/rodape.php'); ?>

</body>
</html>