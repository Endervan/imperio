<?php
ob_start();
session_start();
require_once("../../class/Include.class.php"); 
$obj_site = new Site();


//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
    //  SELECIONO O TIPO
    switch($_GET[tipo])
    {
        case "produto":
            $id = $_GET[id];  
            unset($_SESSION[solicitacoes_produtos][$id]);
            sort($_SESSION[solicitacoes_produtos]);
        break;
        case "servico":
            $id = $_GET[id];  
            unset($_SESSION[solicitacoes_servicos][$id]);
            sort($_SESSION[solicitacoes_servicos]);
        break;
    }
  
}

?>
<!doctype html>
<html>
<head>
<?php require_once('../includes/head.php'); ?>


</head>


</head>

<body class="bg-orcamento">
    

    <?php require_once('../includes/topo.php'); ?>


     <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->
    <div class="container pesquisa-prod-internas">
      <div class="row">
        <div class="col-xs-6 bg-cinza bottom15">
          <img class="top10" src="../imgs/titulo-pesquisar-produtos.jpg" alt="">   
        </div>
        
        <div class="clearfix"></div>

        <?php require_once("../includes/form_busca.php") ?>

      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- pesquisa produto -->
    <!-- ======================================================================= -->


	<div class="container top20">
		<div class="row">
		  <div class="col-xs-12">
      
      
      <?php
        //  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
        if(isset($_POST[nome]))
        {
        
            //  CADASTRO OS PRODUTOS SOLICITADOS
            for($i=0; $i < count($_POST[qtd]); $i++)
            {         
                $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);
                
                $produtos_orcamento .= "
                            <tr>
                                <td><p>". $_POST[qtd][$i] ."</p></td>
                                <td><p>". utf8_encode($dados[titulo]) ."</p></td>
                             </tr>
                            ";
            }
            
           
            
            
            //  ENVIANDO A MENSAGEM PARA O CLIENTE
            $texto_mensagem = "
            O seguinte cliente fez uma solicitação pelo site. <br />


            Tipo: $_POST[tipo_pessoa] <br />
            Nome: $_POST[nome] <br />
            Email: $_POST[email] <br />
            Telefone: $_POST[telefone] <br />
            Bairro: $_POST[bairro] <br />
            Cidade: $_POST[cidade] <br />
            Estado: $_POST[estado] <br />
            Complemento: $_POST[complemento] <br />
            Cep: $_POST[cep] <br />
            Mensagem: <br />
             ". nl2br($_POST[mensagem])." <br />


            <br />
            <h2> Produtos selecionados:</h2> <br />

            <table width='100%' border='0' cellpadding='5' cellspacing='5'>
            
                <tr>
                      <td><h4>QTD</h4></td>
                      <td><h4>PRODUTO</h4></td>
                </tr>

                $produtos_orcamento

            </table>


            </table>
            ";
            
            Util::envia_email($config[email], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            Util::envia_email($config[email_copia], utf8_decode($_POST[nome] . " solicitou um orçamento"), $texto_mensagem, utf8_decode($_POST[nome]), $_POST[email]);
            unset($_SESSION[solicitacoes_produtos]);
            unset($_SESSION[solicitacoes_servicos]);
            Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
             
        }
        ?>



    
      <form class="form-inline FormContato top20" role="form" method="post">

        <h4>MEU ORÇAMENTO (<?php echo count($_SESSION[solicitacoes_produtos]) ?>)</h4>

        <div class="div-tb-lista-itens">
          
          <?php
          if(count($_SESSION[solicitacoes_produtos]) > 0)
          {
          ?>
            <table class="table top20 tb-lista-itens">
              <thead>
                <tr>
                  <th>ITEM</th>
                  <th>DESCRIÇÃO</th>
                  <th class="text-center">QUANTIDADE</th>
                  <th class="text-center">REMOVER</th>
                </tr>
              </thead>
              <tbody>
                <?php
                for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++)
                {
                  $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                  ?>
                  <tr>
                    <td><img src="../../uploads/tumb_<?php Util::imprime($row[imagem]) ?>" height="74" width="48" alt=""></td>
                    <td><h1><?php Util::imprime($row[titulo]) ?></h1>  </td>
                    <td class="text-center">
                      <input type="text" class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                      <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    </td>
                    <td class="text-center">
                      <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir"> 
                        <i class="glyphicon glyphicon-remove link-excluir"></i> 
                      </a>
                      </td>
                  </tr>
                  <?php  
                }
                ?>
              </tbody>
            </table>
          <?php
          }
          ?>  
        </div>

        <div class="row top15">
          <div class="col-xs-12">
            <a href="<?php echo Util::caminho_projeto() ?>/mobile/produtos" title="Continuar orçando" class="btn btn-azul">
              Continuar orçando
            </a> 
          </div>
        </div>


          <div class="titulo-form-orcamento top60">

            <!-- form fale conosco -->
            

              <div class="row top70">
              
                  <div class="col-xs-6 form-group">
                      <label class="glyphicon glyphicon-user"> <span>Nome</span></label>
                      <input type="text" name="nome" class="form-control input100" placeholder="">
                  </div>
                  <div class="col-xs-6 form-group">
                      <label class="glyphicon glyphicon-envelope"> <span>E-mail</span></label>
                      <input type="text" name="email" class="form-control input100" placeholder="">
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-earphone"> <span>Telefone</span></label>
                      <input type="text" name="telefone" class="form-control input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-map-marker"> <span>Estado</span></label>
                      <input type="text" name="estado" class="form-control input100" placeholder="">
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-map-marker"> <span>Cidade</span></label>
                      <input type="text" name="cidade" class="form-control input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-map-marker"> <span>Bairro</span></label>
                      <input type="text" name="bairro" class="form-control input100" placeholder="">
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-6 top20 ">
                      <label class="glyphicon glyphicon-map-marker"> <span>Complemento</span></label>
                      <input type="text" name="complemento" class="form-control input100" placeholder="">
                  </div>
                  <div class="col-xs-6 top20 form-group">
                      <label class="glyphicon glyphicon-map-marker"> <span>Cep</span></label>
                      <input type="text" name="cep" class="form-control input100" placeholder="">
                  </div>

                  <div class="clearfix"></div>

                  <div class="col-xs-12 top20 form-group">
                      <label class="glyphicon glyphicon-pencil"> <span>Mensagem</span></label>
                      <textarea name="mensagem" id=""  rows="10" class="form-control input100"></textarea>
                  </div>

                  <div class="clearfix"></div>

                  <div class="text-right top30 right20">
                      <button type="submit" class="btn btn-azul">
                          ENVIAR
                      </button>
                  </div>
                </div>
            
            <!-- form fale conosco -->
          </div>

      </form>

          


    </div>


      </div> <!-- row -->  
    </div> <!-- container -->
  


    




    















    <?php require_once('../includes/rodape.php'); ?>

</body>
</html>







<script>
    $(document).ready(function() {
        $('.FormContato').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                 nome: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                        
                        },
                        emailAddress: {
                            message: 'Esse endereço de email não é válido'
                        }
                    }
                },
                telefone: {
                    validators: {
                        notEmpty: {
                        
                        }
                    }
                },
                assunto: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                tipo_pessoa: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                cidade: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                estado: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                },
                mensagem: {
                    validators: {
                        notEmpty: {
                            
                        }
                    }
                }
            }
        });
    });
</script>

