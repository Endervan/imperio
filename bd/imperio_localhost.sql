-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 02-Dez-2015 às 15:03
-- Versão do servidor: 5.6.25
-- PHP Version: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `imperio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_avaliacoes_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_avaliacoes_produtos` (
  `idavaliacaoproduto` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL,
  `nota` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_avaliacoes_produtos`
--

INSERT INTO `tb_avaliacoes_produtos` (`idavaliacaoproduto`, `nome`, `email`, `comentario`, `ativo`, `ordem`, `url_amigavel`, `id_produto`, `data`, `nota`) VALUES
(19, 'Joana Dark', ' ', 'Aqui fica o comentário Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standa t ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. d not only five centuries, but also the leap into electronic typesetting,', 'SIM', 0, ' ', 271, '2015-12-02', 3),
(20, 'Marcio', 'marciomas@gmail.com', 'Aqui fica o comentário Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standa t ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. d not only five centuries, but also the leap into electronic typesetting,', 'SIM', 0, ' ', 271, '2015-12-02', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_categorias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_categorias_produtos` (
  `idcategoriaproduto` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desCategoria_Dica_Modelcricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `classe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_categorias_produtos`
--

INSERT INTO `tb_categorias_produtos` (`idcategoriaproduto`, `titulo`, `desCategoria_Dica_Modelcricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `classe`) VALUES
(49, 'FERRAMENTAS ELÉTRICAS', NULL, '0212201510591119118441.png', 'SIM', NULL, 'ferramentas-eletricas', NULL, NULL, NULL, NULL),
(50, 'MOTORES', NULL, '0212201510591372819378.png', 'SIM', NULL, 'motores', NULL, NULL, NULL, NULL),
(51, 'ACESSÓRIOS', NULL, '0212201511001329478861.png', 'SIM', NULL, 'acessorios', NULL, NULL, NULL, NULL),
(52, 'BOMBAS', NULL, '0212201511001304002298.png', 'SIM', NULL, 'bombas', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_comentarios_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_comentarios_dicas` (
  `idcomentariodica` int(11) NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comentario` longtext COLLATE utf8_unicode_ci,
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'NAO',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_dica` int(11) DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_configuracoes`
--

CREATE TABLE IF NOT EXISTS `tb_configuracoes` (
  `idconfiguracao` int(10) unsigned NOT NULL,
  `title_google` longtext NOT NULL,
  `description_google` longtext NOT NULL,
  `keywords_google` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `endereco` varchar(255) NOT NULL,
  `telefone1` varchar(255) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `src_place` varchar(255) NOT NULL,
  `horario_1` varchar(255) DEFAULT NULL,
  `horario_2` varchar(255) DEFAULT NULL,
  `email_copia` varchar(255) DEFAULT NULL,
  `telefone3` varchar(255) DEFAULT NULL,
  `telefone4` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_configuracoes`
--

INSERT INTO `tb_configuracoes` (`idconfiguracao`, `title_google`, `description_google`, `keywords_google`, `ativo`, `ordem`, `url_amigavel`, `endereco`, `telefone1`, `telefone2`, `email`, `src_place`, `horario_1`, `horario_2`, `email_copia`, `telefone3`, `telefone4`) VALUES
(1, '-', '-', '-', 'SIM', 0, '', 'SSTN Edifício Montreal 3 Sala 02 Asa Norte Brasília ? DF', '(61)3026-1117', '(61)3026-3333', 'marciomas@gmail.com', 'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3837.5171072401927!2d-48.0685030854483!3d-15.881957529345042!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x935a2d38d8950121%3A0x424c9714d0b71ec7!2sMastour+-+Aluguel+e+loca%C3%A7%C3%A3o+de+vans%2C', NULL, NULL, '', '(61)3026-4345', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_depoimentos`
--

CREATE TABLE IF NOT EXISTS `tb_depoimentos` (
  `iddepoimento` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `depoimento` longtext,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_depoimentos`
--

INSERT INTO `tb_depoimentos` (`iddepoimento`, `titulo`, `depoimento`, `ativo`, `ordem`, `url_amigavel`, `imagem`) VALUES
(4, 'Jéssica Motta', '<p>\r\n	J&eacute;ssica fala A Imp&eacute;rio Bombas Ltda est&aacute; instalada na cidade de Bras&iacute;lia - DF, h&aacute; 10 anos, atuando co m servi&ccedil;os especializados em bombas d&rsquo;&aacute;gua, motores, ferramentas el&eacute;tricas e vendas de bombas centrifugas e submersas, para piscinas e etc.</p>', 'SIM', NULL, 'jessica-motta', '0112201504171370679190.jpeg'),
(5, 'Maria Aparecida', '<p>\r\n	Maria descreve&nbsp;A Imp&eacute;rio Bombas Ltda est&aacute; instalada na cidade de Bras&iacute;lia - DF, h&aacute; 10 anos, atuando co m servi&ccedil;os especializados em bombas d&rsquo;&aacute;gua, motores, ferramentas el&eacute;tricas e vendas de bombas centrifugas e submersas, para piscinas e etc.</p>', 'SIM', NULL, 'maria-aparecida', '0112201504181255749298..jpg'),
(6, 'Antônia Cruz', '<p>\r\n	Ant&ocirc;nia&nbsp;A Imp&eacute;rio Bombas Ltda est&aacute; instalada na cidade de Bras&iacute;lia - DF, h&aacute; 10 anos, atuando co m servi&ccedil;os especializados em bombas d&rsquo;&aacute;gua, motores, ferramentas el&eacute;tricas e vendas de bombas centrifugas e submersas, para piscinas e etc.</p>', 'SIM', NULL, 'antonia-cruz', '0112201504231282331570..jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_dicas`
--

CREATE TABLE IF NOT EXISTS `tb_dicas` (
  `iddica` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` date DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_dicas`
--

INSERT INTO `tb_dicas` (`iddica`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `data`) VALUES
(42, '7 erros na escolha de portas e janela. Saiba evitá-los', '<p>\r\n	Na constru&ccedil;&atilde;o civil, todo cuidado &eacute; pouco na hora de escolher os materiais que ser&atilde;o comprados. Mesmo no planejamento &eacute; necess&aacute;rio levar em considera&ccedil;&atilde;o o correto uso de portas e janelas, por exemplo. Que s&atilde;o itens que podem apresentar problemas de usabilidade de mal feito. Veja abaixo 7 erros que voc&ecirc; poderia incorrer, mas poder&aacute; evit&aacute;-los.</p>\r\n<p>\r\n	Evite produtos muito baratos<br />\r\n	Comprar portas e janelas baseado apenas no pre&ccedil;o baixo pode ser o primeiro e mais prim&aacute;rio erro que voc&ecirc; pode cometer. As consequ&ecirc;ncias podem ser graves e levar a um desconforto na hora de us&aacute;-los. Portas ou janelas que emperram na abertura, cupins, empenamento, infiltra&ccedil;&atilde;o de &aacute;gua, ferrugem e outros problemas podem ser as consequ&ecirc;ncias.</p>\r\n<p>\r\n	Janela de vidro ou material opaco<br />\r\n	Est&aacute; na moda o uso do vidro, especialmente em janelas. Muitos tem optado por colocar janelas de vidro ou blindex nos quartos. Mas cuidado com esta escolha, pois ela pode ser um problema. O vidro &eacute; transparente e vai passar muita claridade a noite para dentro do quarto, mesmo com o uso de cortinas. Ainda assim considere que uma boa cortina que corte a luz e especialmente os raios ultravioletas do sol pode custar caro.</p>\r\n<p>\r\n	Tamanho da janela<br />\r\n	Atente para o tamanho da janela e para isto considere o espa&ccedil;o de parede, tamanho do quarto e posi&ccedil;&atilde;o da mesma. Quartos muitos grandes com janelas pequenas podem ser esteticamente desagrad&aacute;vel, bem como n&atilde;o promover a ilumina&ccedil;&atilde;o e ventila&ccedil;&atilde;o necess&aacute;ria.</p>\r\n<p>\r\n	Portas de madeira oca e semioca<br />\r\n	Ao comprar portas de madeira fique atento para as folhas de portas ocas e semiocas, aqulas mais baratas. Eu comprei duas portas e ambas apresentaram o mesmo problema na prepara&ccedil;&atilde;o delas para o assentamento, ou seja, ela estufou, pois, &eacute; uma madeirinha bem fininha que reveste a porta. Portas de entrada ou locais onde possa haver umidade escolha madeira maci&ccedil;a.</p>\r\n<p>\r\n	Largura das portas<br />\r\n	Existem v&aacute;rias larguras de portas e &eacute; muito importante ficar atento a isto. Colocar porta muito estreita pode ser um erro grave. Pessoas com cadeiras de rodas podem ter dificuldades de se locomover, assim como a entrada de m&oacute;veis e outros objetos maiores.</p>\r\n<p>\r\n	Posi&ccedil;&atilde;o de portas e janelas<br />\r\n	Outro erro pode estar relacionado &agrave; posi&ccedil;&atilde;o das portas e janelas. Se voc&ecirc; for como eu que gosta de dormir com janelas abertas &eacute; importante pensar na posi&ccedil;&atilde;o dela em rela&ccedil;&atilde;o &agrave; cama para evitar que voc&ecirc; acorde com o sol batendo no seu rosto, assim como a incid&ecirc;ncia de chuva e sol no per&iacute;odo da tarde. Portas de entrada n&atilde;o devem ficar expostas ao sol e chuva se o produto for madeira ou ferro.</p>\r\n<p>\r\n	Acabamento<br />\r\n	Outro erro pode estar relacionado a forma como voc&ecirc; d&aacute; o acabamento, especialmente em madeira. Lembro de quando fui envernizar as portas de uma casa que constru&iacute; e n&atilde;o atentei para o fato de que a madeira arrepia quando se aplica o verniz e lixei a porta para depois aplicar o verniz. O resultado n&atilde;o ficou bom. O ideal &eacute; chamar um profissional de pintura para fazer um acabamento de qualidade, especialmente em portas e janelas de madeira.</p>', '0112201505241293545282..jpg', 'SIM', NULL, '7-erros-na-escolha-de-portas-e-janela-saiba-evitalos', NULL, NULL, NULL, NULL),
(43, 'Portas e janelas de madeira. Vantagens e desvantagens', '<p>\r\n	As portas e janelas de uma casa s&atilde;o elementos importantes no projeto de constru&ccedil;&atilde;o ou reforma de casas e apartamentos. Existem diversos tipos de materiais que podem ser usados como ferro, alum&iacute;nio, PVC, vidro, madeira e outros. A prefer&ecirc;ncia pela madeira &eacute; grande gra&ccedil;as a boa est&eacute;tica, durabilidade e outros. Mas este material exige cuidados e abaixo listarei algumas das principais vantagens e desvantagens de portas e janelas de madeira.</p>\r\n<p>\r\n	Vantagens<br />\r\n	Beleza<br />\r\n	A madeira &eacute; bela, sofisticada e proporciona uma excelente harmonia com a estrutura da casa, seja ela alvenaria, gesso e outros materiais. Por ser um material f&aacute;cil de trabalhar existem muitos tipos de portas e especialmente de janelas com muitos detalhes trabalhados e entalhados na madeira. De fato fica muito bonito uma casa com janelas e portas de madeira.</p>\r\n<p>\r\n	Durabilidade<br />\r\n	&Eacute; um material dur&aacute;vel, contudo e como ser&aacute; visto abaixo a dura&ccedil;&atilde;o dele depende muito do cuidado que ele receber&aacute;. A durabilidade deste material pode ser comprovada quando fazemos a demoli&ccedil;&atilde;o de casas antigas, onde tudo est&aacute; deteriorado, mas a madeira que n&atilde;o ficou exposta a chuva permanece firme.</p>\r\n<p>\r\n	Resist&ecirc;ncia<br />\r\n	&Eacute; tamb&eacute;m um material muito resistente, haja vista que ele &eacute; usado para construir a estrutura do telhado das casas. Evidentemente que no caso de janelas, especialmente &eacute; preciso analisar como a madeira foi usada e o modelo da janela, mas em geral &eacute; bastante resistente.</p>\r\n<p>\r\n	Desvantagens<br />\r\n	Limpeza<br />\r\n	Como desvantagem podemos citar a quest&atilde;o da limpeza, pois &eacute; um material que n&atilde;o pode ser molhado e no caso de janelas especialmente, gra&ccedil;as aos muitos detalhes, muitas tornam-se&nbsp; de dif&iacute;cil limpeza. Lembro que em uma antiga casa minha, coloquei uma janela de madeira toda quadriculada com muitos vidros pequenos. Era dif&iacute;cil para fazer a limpeza, pois tinha de limpar peda&ccedil;o por peda&ccedil;o. Pense nisso na hora de escolher o modelo.</p>\r\n<p>\r\n	Umidade<br />\r\n	A umidade &eacute; o grande problema da madeira e por este motivo n&atilde;o &eacute; recomend&aacute;vel usar em &aacute;reas expostas a chuva ou que possa ter incid&ecirc;ncia de &aacute;gua. Geralmente nas janelas expostas &eacute; colocado um toldo em cima para evitar o contato com a chuva. A durabilidade da madeira depender&aacute; muito deste cuidado com a umidade.</p>\r\n<p>\r\n	Cupins<br />\r\n	A madeira pode ser atacada tamb&eacute;m por cupins e quando isso acontece se os devidos cuidados n&atilde;o forem tomados as consequ&ecirc;ncias&nbsp; a longo prazo ser&atilde;o ruins o que pode levar a deteriora&ccedil;&atilde;o da pe&ccedil;a. Uma recomenda&ccedil;&atilde;o neste caso &eacute; evitar madeiras muito moles ou porosas que s&atilde;o as mais f&aacute;ceis de serem atacadas.</p>', '0112201505251321201647..jpg', 'SIM', NULL, 'portas-e-janelas-de-madeira-vantagens-e-desvantagens', NULL, NULL, NULL, NULL),
(44, 'Concreto usinado para a laje. Preço e cálculo de quantidade', '<p>\r\n	Usar concreto usinado na constru&ccedil;&atilde;o de casas residenciais tornou-se comum e as vantagens s&atilde;o muitas, especialmente em algumas etapas da constru&ccedil;&atilde;o onde seu uso parece fazer mais sentido. Um desses locais &eacute; sem d&uacute;vida nenhuma a concretagem da laje da casa.</p>\r\n<p>\r\n	Raz&otilde;es para o uso do concreto usinado<br />\r\n	A concretagem da laje de uma casa precisa ser feita de uma &uacute;nica vez e com isto n&atilde;o &eacute; preciso dizer que qualquer m&eacute;todo que torne o processo mais r&aacute;pido &eacute; muito bem-vindo. Imagine uma casa de 150 m/2 a quantidade de concreto que precisaria ser feito para realizar todo o trabalho?</p>\r\n<p>\r\n	Portanto, o concreto usinado &eacute; muito usado para concretar laje exatamente pelo fato dele vir pronto no caminh&atilde;o, bastando a aplica&ccedil;&atilde;o dele. Com isso ganha-se tempo e permite que em apenas algumas horas, toda a laje de uma casa fique pronta.</p>\r\n<p>\r\n	Calculo da quantidade<br />\r\n	Mas como saber qual a quantidade de concreto devo comprar para a laje de uma casa? Bem, existe um c&aacute;lculo que geralmente as empresas que vendem o produto j&aacute; faz, com isto basta voc&ecirc; informar a metragem da laje e a finalidade do concreto que eles j&aacute; t&ecirc;m a experi&ecirc;ncia de calcular e mandar a quantidade certa.</p>\r\n<p>\r\n	Contudo o c&aacute;lculo &eacute; muito simples, pois o concreto &eacute; vendido em m/3 (metros c&uacute;bicos), desta forma basta aplicar esta f&oacute;rmula: Comprimento x altura x largura.</p>\r\n<p>\r\n	Exemplo: 15 x 8 x 0,12 = 14,4</p>\r\n<p>\r\n	Ou seja, voc&ecirc; considera a largura (15 metros), o comprimento (8 metros) e a altura (12 cent&iacute;metros) e ai ter&aacute; a quantidade em metros c&uacute;bicos, que no exemplo deu 14,4 m/3.</p>\r\n<p>\r\n	Pre&ccedil;o do concreto usinado<br />\r\n	Sei que o pre&ccedil;o dele deve variar um pouco de regi&atilde;o para regi&atilde;o. Na minha cidade que fica na regi&atilde;o de Campinas no interior de S&atilde;o Paulo o pre&ccedil;o tem variado de R$ 210,00 a R$ 230,00 m/3.</p>\r\n<p>\r\n	Custo da bomba<br />\r\n	No caso de concreto para a laje existe ainda mais um custo que &eacute; o da bomba, afinal mandar o concreto do caminh&atilde;o para a laje requer uma m&aacute;quina para bombear o concreto. O custo gira em torno de R$ 500,00 para fazer o servi&ccedil;o e geralmente a mesma empresa que fornece o concreto tamb&eacute;m aluga a bomba.</p>\r\n<p>\r\n	Conclus&atilde;o<br />\r\n	Usar concreto usinado torna o processo mais r&aacute;pido e deixa a concretagem mais uniforme, garantindo uma qualidade melhor. Em termos de custo eu n&atilde;o fiz o c&aacute;lculo, mas acredito que deve ficar no mesmo pre&ccedil;o se voc&ecirc; for fazer o concreto, j&aacute; quem tem de considerar cimento, pedra, areia, m&atilde;o de obra e outros.</p>', '0112201505261371073289..jpg', 'SIM', NULL, 'concreto-usinado-para-a-laje-preco-e-calculo-de-quantidade', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_empresa`
--

CREATE TABLE IF NOT EXISTS `tb_empresa` (
  `idempresa` int(10) unsigned NOT NULL,
  `titulo` varchar(80) NOT NULL,
  `descricao` longtext NOT NULL,
  `ativo` varchar(3) NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `title_google` varchar(255) NOT NULL,
  `keywords_google` varchar(255) NOT NULL,
  `description_google` varchar(255) NOT NULL,
  `url_amigavel` varchar(255) NOT NULL,
  `colaboradores_especializados` int(11) DEFAULT NULL,
  `trabalhos_entregues` int(11) DEFAULT NULL,
  `trabalhos_entregues_este_mes` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_empresa`
--

INSERT INTO `tb_empresa` (`idempresa`, `titulo`, `descricao`, `ativo`, `ordem`, `title_google`, `keywords_google`, `description_google`, `url_amigavel`, `colaboradores_especializados`, `trabalhos_entregues`, `trabalhos_entregues_este_mes`) VALUES
(1, 'A EMPRESA', '<p>\r\n	A Imp&eacute;rio Bombas Ltda est&aacute; instalada na cidade de Bras&iacute;lia - DF, h&aacute; 10 anos, atuando com servi&ccedil;os especializados em bombas d&rsquo;&aacute;gua, motores, ferrament as el&eacute;tricas e vendas de bombas centrifugas e submersas, para piscinas e etc.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Atendemos toda linha de: bombas d&rsquo;&aacute;gua, ferramentas el&eacute;tricas, motores, lava doras de alta press&atilde;o, cortadores de grama, geradores, ro&ccedil;adeiras, quadro de comando, compressores, rebobinamento em motores dentre outros.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	A empresa busca incessantemente a qualidade total de seus servi&ccedil;os. Devido a seriedade, ao empenho no trabalho e &agrave; idealiza&ccedil;&atilde;o das metas a alcan&ccedil;ar, a Imp&eacute;rio Bombas Ltda direcionou todos os esfor&ccedil;os no intuito de conseguir o pleno desenvolvimento da qualidade em todos os nossos servi&ccedil;os prestados , com o objetivo de satisfazer a exig&ecirc;ncia de nossos clientes.</p>', 'SIM', 0, '', '', '', 'a-empresa', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_galerias_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_galerias_produtos` (
  `id_galeriaproduto` int(11) NOT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) DEFAULT NULL,
  `id_produto` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tb_galerias_produtos`
--

INSERT INTO `tb_galerias_produtos` (`id_galeriaproduto`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `id_produto`) VALUES
(60, '0212201501381390885273.jpg', 'SIM', NULL, NULL, 271),
(61, '0212201501381285363964.jpg', 'SIM', NULL, NULL, 271),
(62, '0212201501381282294850.jpeg', 'SIM', NULL, NULL, 271),
(63, '0212201501381364649303.jpg', 'SIM', NULL, NULL, 271),
(64, '0212201501381130877473.jpg', 'SIM', NULL, NULL, 272),
(65, '0212201501381281034835.jpeg', 'SIM', NULL, NULL, 271),
(66, '0212201501381168096731.jpg', 'SIM', NULL, NULL, 272),
(67, '0212201501381152642360.jpg', 'SIM', NULL, NULL, 273),
(68, '0212201501381197379550.jpeg', 'SIM', NULL, NULL, 272),
(69, '0212201501381255460342.jpg', 'SIM', NULL, NULL, 273),
(70, '0212201501381328314283.jpg', 'SIM', NULL, NULL, 272),
(71, '0212201501381292061851.jpg', 'SIM', NULL, NULL, 274),
(72, '0212201501381353876272.jpeg', 'SIM', NULL, NULL, 272),
(73, '0212201501381351933850.jpeg', 'SIM', NULL, NULL, 273),
(74, '0212201501381249459735.jpg', 'SIM', NULL, NULL, 274),
(75, '0212201501381354374817.jpg', 'SIM', NULL, NULL, 273),
(76, '0212201501381255182087.jpg', 'SIM', NULL, NULL, 275),
(77, '0212201501381224975774.jpg', 'SIM', NULL, NULL, 275),
(78, '0212201501381151492472.jpeg', 'SIM', NULL, NULL, 275),
(79, '0212201501381399140490.jpeg', 'SIM', NULL, NULL, 273),
(80, '0212201501381287235553.jpeg', 'SIM', NULL, NULL, 274),
(81, '0212201501381253398762.jpg', 'SIM', NULL, NULL, 274),
(82, '0212201501381296130187.jpg', 'SIM', NULL, NULL, 275),
(83, '0212201501381137119340.jpeg', 'SIM', NULL, NULL, 275),
(84, '0212201501381362097518.jpeg', 'SIM', NULL, NULL, 274);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logins` (
  `idlogin` int(11) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `senha` varchar(45) DEFAULT NULL,
  `ativo` varchar(3) DEFAULT 'SIM',
  `id_grupologin` int(11) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logins`
--

INSERT INTO `tb_logins` (`idlogin`, `nome`, `senha`, `ativo`, `id_grupologin`, `email`) VALUES
(5, 'HomeWeb', 'e10adc3949ba59abbe56e057f20f883e', 'SIM', 0, 'atendimento.sites@homewebbrasil.com.br');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_logs_logins`
--

CREATE TABLE IF NOT EXISTS `tb_logs_logins` (
  `idloglogin` int(11) NOT NULL,
  `operacao` longtext,
  `consulta_sql` longtext,
  `data` date DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `id_login` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1608 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `tb_logs_logins`
--

INSERT INTO `tb_logs_logins` (`idloglogin`, `operacao`, `consulta_sql`, `data`, `hora`, `id_login`) VALUES
(1580, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '16:02:55', 5),
(1581, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '16:04:09', 5),
(1582, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '16:17:57', 5),
(1583, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '16:18:25', 5),
(1584, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '16:18:41', 5),
(1585, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '16:23:15', 5),
(1586, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '17:24:58', 5),
(1587, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '17:25:36', 5),
(1588, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '17:26:29', 5),
(1589, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '18:58:15', 5),
(1590, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '18:59:32', 5),
(1591, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '23:07:32', 5),
(1592, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '23:08:28', 5),
(1593, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '23:09:27', 5),
(1594, 'CADASTRO DO CLIENTE ', '', '2015-12-01', '23:10:13', 5),
(1595, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-01', '23:20:09', 5),
(1596, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '10:59:41', 5),
(1597, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '10:59:59', 5),
(1598, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '11:00:10', 5),
(1599, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '11:00:34', 5),
(1600, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '12:44:32', 5),
(1601, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '12:45:33', 5),
(1602, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '12:53:00', 5),
(1603, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '12:54:11', 5),
(1604, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '12:54:48', 5),
(1605, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-02', '14:15:12', 5),
(1606, 'CADASTRO DO CLIENTE ', '', '2015-12-02', '14:17:06', 5),
(1607, 'ALTERAÇÃO DO CLIENTE ', '', '2015-12-02', '14:18:25', 5);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_produtos`
--

CREATE TABLE IF NOT EXISTS `tb_produtos` (
  `idproduto` int(10) unsigned NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci NOT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ativo` varchar(3) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'SIM',
  `ordem` int(10) unsigned NOT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_categoriaproduto` int(10) unsigned NOT NULL,
  `marca` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `apresentacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `avaliacao` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caracteristicas` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=277 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_produtos`
--

INSERT INTO `tb_produtos` (`idproduto`, `titulo`, `imagem`, `descricao`, `title_google`, `keywords_google`, `description_google`, `ativo`, `ordem`, `url_amigavel`, `id_categoriaproduto`, `marca`, `apresentacao`, `avaliacao`, `caracteristicas`) VALUES
(271, 'Bombas Para Piscinas', '0212201512441394790788.jpeg', '<p>\r\n	Bomba para piscaina s&atilde;o&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'bombas-para-piscinas', 52, NULL, NULL, NULL, ''),
(272, 'Filtro Para Piscina', '0212201512451399717100..jpg', '<p>\r\n	Filtro Para Piscina&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'filtro-para-piscina', 51, NULL, NULL, NULL, NULL),
(273, 'Bomba De Água Submersa', '0212201512521320968066..jpg', '<p>\r\n	Bomba De &Aacute;gua Submersa&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'bomba-de-agua-submersa', 52, NULL, NULL, NULL, NULL),
(274, 'Steel Motor - Monofásico', '0212201512541227929011.jpeg', '<p>\r\n	Steel&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'steel-motor--monofasico', 50, NULL, NULL, NULL, NULL),
(275, 'MOTOR MONOFÁSICO 4 PÓLOS ABERTO', '0212201512541360223311..jpg', '<p>\r\n	MOTOR MONOF&Aacute;SICO 4 P&Oacute;LOS ABERTO&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'motor-monofasico-4-polos-aberto', 50, NULL, NULL, NULL, NULL),
(276, 'Furadeira Eletrica Reversivel', '0212201502171279350518..jpg', '<p>\r\n	Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '', '', '', 'SIM', 0, 'furadeira-eletrica-reversivel', 49, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_servicos`
--

CREATE TABLE IF NOT EXISTS `tb_servicos` (
  `idservico` int(11) NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `descricao` longtext COLLATE utf8_unicode_ci,
  `imagem` varchar(45) COLLATE utf8_unicode_ci DEFAULT 'imagem_nao_disponivel.jpg',
  `ativo` varchar(3) COLLATE utf8_unicode_ci DEFAULT 'SIM',
  `ordem` int(11) DEFAULT NULL,
  `url_amigavel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `keywords_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_google` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_categoriaservico` int(10) unsigned NOT NULL,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=41 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `tb_servicos`
--

INSERT INTO `tb_servicos` (`idservico`, `titulo`, `descricao`, `imagem`, `ativo`, `ordem`, `url_amigavel`, `title_google`, `keywords_google`, `description_google`, `id_categoriaservico`, `imagem_1`, `imagem_2`) VALUES
(37, 'Construco reformas e terraplanagem', '<p>\r\n	Constru&ccedil;&atilde;o reforma e&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0112201511071337038231..jpg', 'SIM', NULL, 'construco-reformas-e-terraplanagem', '', '', '', 0, '', ''),
(38, 'Pintura residencial', '<p>\r\n	Pintura residencial&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0112201511081257823533.jpeg', 'SIM', NULL, 'pintura-residencial', '', '', '', 0, '', ''),
(39, 'Limpeza de vidros janela vidraca', '<p>\r\n	Limpeza de vidros janela vidraca&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0112201511091380365489..jpg', 'SIM', NULL, 'limpeza-de-vidros-janela-vidraca', '', '', '', 0, '', ''),
(40, 'Instalação Forro em Gesso Acartonado', '<p>\r\n	Instala&ccedil;&atilde;o Forro em Gesso Acartonado&nbsp;Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>', '0112201511201173121022..jpg', 'SIM', NULL, 'instalacao-forro-em-gesso-acartonado', '', '', '', 0, '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  ADD PRIMARY KEY (`idavaliacaoproduto`);

--
-- Indexes for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  ADD PRIMARY KEY (`idcategoriaproduto`);

--
-- Indexes for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  ADD PRIMARY KEY (`idcomentariodica`);

--
-- Indexes for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  ADD PRIMARY KEY (`idconfiguracao`);

--
-- Indexes for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  ADD PRIMARY KEY (`iddepoimento`);

--
-- Indexes for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  ADD PRIMARY KEY (`iddica`);

--
-- Indexes for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  ADD PRIMARY KEY (`idempresa`);

--
-- Indexes for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  ADD PRIMARY KEY (`id_galeriaproduto`);

--
-- Indexes for table `tb_logins`
--
ALTER TABLE `tb_logins`
  ADD PRIMARY KEY (`idlogin`);

--
-- Indexes for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  ADD PRIMARY KEY (`idloglogin`);

--
-- Indexes for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  ADD PRIMARY KEY (`idproduto`);

--
-- Indexes for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  ADD PRIMARY KEY (`idservico`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_avaliacoes_produtos`
--
ALTER TABLE `tb_avaliacoes_produtos`
  MODIFY `idavaliacaoproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_categorias_produtos`
--
ALTER TABLE `tb_categorias_produtos`
  MODIFY `idcategoriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tb_comentarios_dicas`
--
ALTER TABLE `tb_comentarios_dicas`
  MODIFY `idcomentariodica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_configuracoes`
--
ALTER TABLE `tb_configuracoes`
  MODIFY `idconfiguracao` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_depoimentos`
--
ALTER TABLE `tb_depoimentos`
  MODIFY `iddepoimento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_dicas`
--
ALTER TABLE `tb_dicas`
  MODIFY `iddica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_empresa`
--
ALTER TABLE `tb_empresa`
  MODIFY `idempresa` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_galerias_produtos`
--
ALTER TABLE `tb_galerias_produtos`
  MODIFY `id_galeriaproduto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT for table `tb_logins`
--
ALTER TABLE `tb_logins`
  MODIFY `idlogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_logs_logins`
--
ALTER TABLE `tb_logs_logins`
  MODIFY `idloglogin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1608;
--
-- AUTO_INCREMENT for table `tb_produtos`
--
ALTER TABLE `tb_produtos`
  MODIFY `idproduto` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=277;
--
-- AUTO_INCREMENT for table `tb_servicos`
--
ALTER TABLE `tb_servicos`
  MODIFY `idservico` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
