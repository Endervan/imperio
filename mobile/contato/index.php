<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE

$description = $config[description_google];
$keywords = $config[keywords_google];
$titulo_pagina = $config[title_google];
?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>

    
    
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>


</head>

<body class="bg-interna">


<?php require_once("../includes/topo.php") ?>

<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row clearfix ">
      <div class="">
          <br>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg-fale-conosco.jpg"
          width="380"
          height="160"
          layout="responsive"
          alt="AMP"></amp-img>
      </div>
  </div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->




<div class="row bottom30 fundo_contatos">
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->
    <div class="col-12">


        <?php
        //  VERIFICO SE E PARA ENVIAR O EMAIL
        if (isset($_GET[nome])) {
            $texto_mensagem = "
            Assunto: " . ($_GET[assunto]) . " <br />
            Nome: " . ($_GET[nome]) . " <br />
            Email: " . ($_GET[email]) . " <br />
            Telefone: " . ($_GET[telefone]) . " <br />
            Celular: " . ($_GET[celular]) . " <br />


            Mensagem: <br />
            " . (nl2br($_GET[mensagem])) . "
            ";

            if (Util::envia_email($config[email], ("$_GET[nome] solicitou contato pelo site"), ($texto_mensagem), ($_GET[nome]), $_GET[email])) {
                Util::envia_email($config[email_copia], ("$_GET[nome] solicitou contato pelo site"), ($texto_mensagem), ($_GET[nome]), $_GET[email]);
                $enviado = 'sim';
                unset($_GET);
            }


        }
        ?>


        <?php if ($enviado == 'sim'): ?>
            <div class="col-12  text-center top50">
                <h4>Email enviado com sucesso.</h4>
            </div>
        <?php else: ?>


            <form method="get" class="p2" action-xhr="index.php" target="_top">


                <div class="ampstart-input top10 inline-block">

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="assunto"
                               placeholder="ASSUNTO" required>
                        <span class="fa fa-user-circle form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="nome" placeholder="NOME"
                               required>
                        <span class="fa fa-user form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="email" class="input-form input100 block border-none" name="email"
                               placeholder="EMAIL" required>
                        <span class="fa fa-envelope form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="tel" class="input-form input100 block border-none" name="telefone"
                               placeholder="TELEFONE" required>
                        <span class="fa fa-phone form-control-feedback"></span>
                    </div>

                    <div class="relativo">
                        <input type="text" class="input-form input100 block border-none" name="celular"
                               placeholder="CELULAR">
                        <span class="fa fa-mobile-phone form-control-feedback"></span>
                    </div>


                    <div class="relativo">
                        <textarea name="mensagem" placeholder="MENSAGEM"
                                  class="input-form input100 campo-textarea"></textarea>
                        <span class="fa fa-pencil form-control-feedback"></span>
                    </div>

                </div>

                <div class="col-12 padding0">
                    <input type="submit" value="ENVIAR ORÇAMENTO" class="btn btn-verde btn-block padding0">
                    
                </div>


                <div submit-success>
                    <template type="amp-mustache">
                        Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
                    </template>
                </div>

                <div submit-error>
                    <template type="amp-mustache">
                        Houve um erro, {{name}} por favor tente novamente.
                    </template>
                </div>

            </form>

        <?php endif; ?>
    </div>
    <!-- ======================================================================= -->
    <!-- FORMULARIO   -->
    <!-- ======================================================================= -->

</div>



<div class="row bottom20">
    <div class="col-12 top30">
        <a class="btn btn-block padding0 btn-default"
           on="tap:my-lightbox444"
           role="a"
           tabindex="0">
           <i class="fa fa-phone mr-1" aria-hidden="true"></i>LIGAR AGORA
        </a>
    </div>
</div>

<!-- <div class="row empresa_geral top35">
    <div class="col-12 text-center">
        <h4>NOSSA LOCALIZAÇÃO</h4>
        <amp-img class="top10" src="<?php //echo Util::caminho_projeto(); ?>/mobile/imgs/barra_contato.png" alt=""
                 height="24" width="217"></amp-img>

    </div>


</div> -->


 <?php require_once("../includes/unidades.php"); ?>

<?php //require_once("../includes/veja.php") ?>


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
