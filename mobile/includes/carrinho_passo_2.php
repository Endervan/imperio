<?php

# ==============================================================  #
# VERIFICO SE FOI ENVIADO A OS DADOS
# ==============================================================  #
if(isset($_GET[btn_cadastrar])):
  $obj_carrinho->armazena_mensagem_endereco_entrega($_GET);

  $caminho = Util::caminho_projeto(). "/mobile/pagamento";
  header("location: $caminho ");

endif;

?>


<!--  ==============================================================  -->
<!-- MENU -->
<!--  ==============================================================  -->
 <div class="row">
    <div class="col-12">
        <ul class="nav  top15">
            <li class="input100">
                <button type="button" class=" btn btn-lg btn-block btn_1"><i class="fa fa-user right20" aria-hidden="true"></i> ENDEREÇO</button>      
            </li>
        </ul>
    </div>
</div>
<!--  ==============================================================  -->
<!-- MENU -->
<!--  ==============================================================  -->




<!--  ==============================================================  -->
<!-- LOCAL DA ENTREGA-->
<!--  ==============================================================  -->
<div class="row">

<div class="col-12 padding0">
  <form method="get" action="#" target="_top">


    <div class="ampstart-input inline-block  form_contatos m0 p0 mb3">




      <div class="col-12">
        <div class="relativo  top20">
          <input type="text" name="nome" class="input-form input100 block border-none p0 m0" placeholder="NOME" required value="<?php echo ( $_SESSION[entrega][nome]); ?>">
          <span class="fa fa-user form-control-feedback"></span>
        </div>
      </div>


      <div class="col-12">
        <div class="relativo  top10">
          <input type="email" name="email" class="input-form input100 block border-none p0 m0" placeholder="EMAIL" required value="<?php echo ( $_SESSION[entrega][email]) ?>">
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>
      </div>


      <div class="col-6 top10">
        <div class="relativo ">
          <input type="text" name="telefone_contato" class="input-form input100 block border-none p0 m0" placeholder="TELEFONE" value="<?php echo ( $_SESSION[entrega][telefone_contato] ); ?>">
          <span class="fa fa-phone form-control-feedback"></span>

        </div>
      </div>


      <div class="col-6 top10">
        <div class="relativo  ">
          <input type="text" name="celular_contato" class="input-form input100 block border-none p0 m0" placeholder="CELULAR " required value="<?php echo ( $_SESSION[entrega][celular_contato] ); ?>">
          <span class="fa fa-mobile form-control-feedback"></span>
        </div>
      </div>




      <div class="col-5 top10">
        <div class="relativo  ">
          <input type="text" maxlength="8" name="cep_entrega" class="input-form input100 block border-none p0 m0"  required disabled placeholder="<?php Util::imprime($_SESSION[dados_entrega][cep]); ?>">
          <span class="fa fa-map-marker form-control-feedback"></span>
        </div>
      </div>


      <div class="col-7 top10">
        <div class="relativo  ">
          <input type="text" name="endereco_entrega" class="input-form input100 block border-none p0 m0"  placeholder="<?php Util::imprime($_SESSION[dados_entrega][logradouro]); ?>" disabled>
          <span class="fa fa-address-book form-control-feedback"></span>
        </div>
      </div>

      <div class="col-4 top10">
        <div class="relativo  ">
          <input type="text" name="numero_entrega" class="input-form input100 block border-none p0 m0" placeholder="NR" required value="<?php echo ( $_SESSION[entrega][numero_entrega] ); ?>" >
          <span class="fa fa-calculator form-control-feedback"></span>

        </div>
      </div>

      <div class="col-8 top10">
        <div class="relativo ">
          <input type="text" name="complemento_entrega" class="input-form input100 block border-none p0 m0" placeholder="COMPLEMENTO" value="<?php echo ( $_SESSION[entrega][complemento_entrega] ); ?>">
          <span class="fa fa-list form-control-feedback"></span>
        </div>
      </div>


      <div class="col-12 top10">
        <div class="relativo ">
          <input type="text" name="ponto_referencia" class="input-form input100 block border-none p0 m0" placeholder="PONTO DE REFERÊNCIA" value="<?php echo ( $_SESSION[entrega][ponto_referencia] ); ?>" >
          <span class="fa fa-map-marker form-control-feedback"></span>

        </div>
      </div>

      <div class="col-12 top10">
        <div class="relativo ">
          <input type="text" name="cidade" disabled class="input-form input100 block border-none p0 m0" placeholder="<?php Util::imprime($_SESSION[dados_entrega][bairro]); ?>">
          <span class="fa fa-list form-control-feedback"></span>
        </div>
      </div>


      <div class="col-9 top10">
        <div class="relativo ">
          <input type="text" name="cidade" disabled class="input-form input100 block border-none p0 m0" placeholder="<?php Util::imprime($_SESSION[dados_entrega][localidade]); ?>">
          <span class="fa fa-list form-control-feedback"></span>
        </div>
      </div>


      <div class="col-3 top10">
        <div class="relativo ">
          <input type="text" name="uf" disabled class="input-form input100 block border-none p0 m0" placeholder="<?php Util::imprime($_SESSION[dados_entrega][uf]); ?>">
          <span class="fa fa-list form-control-feedback"></span>
        </div>
      </div>






      

      <div class="clearfix"></div>

      </div>


      <div class="col-12 text-right top10">
        <input type="submit" name="btn_cadastrar" id="btn_enviar" class="btn btn_1 input100" value="CONTINUAR"  />
      </div>

    </form>


</div>
</div>
<!--  ==============================================================  -->
<!-- LOCAL DA ENTREGA-->
<!--  ==============================================================  -->

    