<?php
$ids = explode("/", $_GET[get1]);

require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$description = $config[description_google];
$keywords = $config[keywords_google];
$titulo_pagina = $config[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
    <?php require_once("../includes/head.php"); ?>
    <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

    <style amp-custom>
        <?php require_once("../css/geral.css"); ?>
        <?php require_once("../css/topo_rodape.css"); ?>
        <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

        .bg-interna {
            background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 60px center no-repeat;
            background-size: 100% 128px;
        }
    </style>


    <script async custom-element="amp-list" src="https://cdn.ampproject.org/v0/amp-list-0.1.js"></script>
    <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
    <script async custom-element="amp-iframe" src="https://cdn.ampproject.org/v0/amp-iframe-0.1.js"></script>

</head>

<body class="bg-interna">

<?php
$voltar_para = ''; // link de volta, exemplo produtos, dicas, servicos etc
require_once("../includes/topo.php") ?>


<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->
<div class="row clearfix ">
      <div class="">
          <br>
          <amp-img src="<?php echo Util::caminho_projeto(); ?>/mobile/imgs/bg-onde-estamos.jpg"
          width="380"
          height="160"
          layout="responsive"
          alt="AMP"></amp-img>
      </div>
  </div>
<!-- ======================================================================= -->
<!-- TITULO PAGINA  -->
<!-- ======================================================================= -->




 <?php require_once("../includes/unidades.php"); ?>





<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->
<?php require_once("../includes/veja.php") ?>
<!--  ==============================================================  -->
<!--   VEJA TAMBEM -->
<!--  ==============================================================  -->


<?php require_once("../includes/rodape.php") ?>

</body>


</html>
